/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */
class Play extends Interface {

    /**
     * Constructeur de l'ihm de la création d'exercice
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {TrainMode} refTrainMode la référence au mode exercer pour tester les jeux
     */
    constructor(refGame, refTrainMode) {

        super(refGame, "play");

        this.exercices = {};

        this.selectedGameId = -1;

        this.refTrainMode = refTrainMode;

        this.elements = {
            title: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),
            scrollPane: new ScrollPane(0, 60, 600, 480),
            btn: new Button(300, 570, '', 0x0088FF, 0x000000, true)
        };
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 30;

        this.elements.btn.setOnClick(function () {
            if (this.selectedGameId < 0)
                return;
            this.playExerice(this.selectedGameId);
        }.bind(this));
    }

    init() {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        super.init();
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();

        this.exercices = this.refGame.global.resources.getExercicesEleves();
        if (this.exercices) this.exercices = JSON.parse(this.exercices);


        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.setElements(this.elements);

        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        this.generateScrollPane(lang);
        this.refGame.showText(this.refGame.global.resources.getTutorialText("play"));
        this.elements.btn.setText(this.refGame.global.resources.getOtherText('playBtn'));
    }


    playExerice(exId) {
        this.refGame.trainMode.init(false, 'playMode');
        this.refGame.trainMode.show(this.exercices[exId].exercice);
    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    updateFont(isOpendyslexic) {
        let font = isOpendyslexic ? 'Opendyslexic' : 'Arial';
    }

    generateScrollPane(lang) {
        if (!this.exercices)
            return;
        this.elements.scrollPane.clear();
        let tg = new ToggleGroup('single');
        for (let exID of Object.keys(this.exercices)) {
            let ex = this.exercices[exID];
            let label = (ex.exercice.name[lang] ? ex.exercice.name[lang] : '') + ex.name;
            let btn = new ToggleButton(0, 0, label, false, 580);
            btn.enable();
//            btn = new Button(0, 0, label, 0x00AA00, 0xFFFFFF, false, 580);

            tg.addButton(btn);

            btn.setOnClick(function () {
                this.selectedGameId = exID;
            }.bind(this));
            this.elements.scrollPane.addElements(btn);
        }
        this.elements.scrollPane.init();
    }
}
