/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */
class Create extends Interface {


    constructor(refGame) {
        super(refGame, "create");

        this.currentTutorialText = 'create';

        this.refGame = refGame;
        this.options = {};
        this.createdCharacter = [];

        this.names = {
            fr: '',
            de: '',
            en: '',
            it: ''
        };

        this.intruders = [];
        this.intrudersList = [];

        this.tree = new Tree(0, 0, 600, 600, this.refGame.global.resources.getImage('tree'));
        this.mainTitle = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
        this.mainTitle.anchor.set(0.5);
        this.mainTitle.x = 300;
        this.mainTitle.y = 150;
        this.startButton.setOnClick(this.showTree.bind(this));

        this.endButton = new Button(525, 585, '', 0x007bff, 0xFFFFFF, true);
        this.endButton.setOnClick(this.endInsertion.bind(this));

        this.nameButton = new Button(525, 585, '', 0x007bff, 0xFFFFFF, true);
        this.nameButton.setOnClick(this.displayName.bind(this));

        this.editNameButton = new Button(300, 100, '', 0x028700, 0xFFFFFF, true);
        this.editNameButton.setOnClick(this.editName.bind(this));

        this.nextBtn = new Button(525, 585, '', 0x028700, 0xFFFFFF, true);
        this.nextBtn.setOnClick(this.showOptions.bind(this));
        this.addIntruderBtn = new Button(300, 585, '', 0x028700, 0xFFFFFF, true);
        this.addIntruderBtn.setOnClick(this.addIntruder.bind(this));

        this.characters = {};

        this.pages = {
            submission: {
                submissionTitle: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }),
                btnSubmit: new Button(300, 200, '', 0x28a745, 0xFFFFFF, false, 250),
                btnDelete: new Button(300, 300, '', 0xdc3545, 0xFFFFFF, false, 250),
                btnTest: new Button(300, 400, '', 0x007bff, 0xFFFFFF, false, 250),
                backToName: new Button(75, 585, '', 0x007bff, 0xFFFFFF, true)
            }, name: {
                fr: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }), de: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }), en: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }), it: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }),
                backToOptions: new Button(75, 585, '', 0x007bff, 0xFFFFFF, true)
            },
            options: {
                showLastName: new CheckBox(150, 150, '', 0.9),
                showFirstName: new CheckBox(150, 200, '', 0.9),
                showAge: new CheckBox(150, 250, '', 0.9),
                showBirthdate: new CheckBox(150, 300, '', 0.9),
                showDeathdate: new CheckBox(150, 350, '', 0.9),
                showAddress: new CheckBox(150, 400, '', 0.9),
                showParents: new CheckBox(150, 450, '', 0.9),
                showChild: new CheckBox(150, 500, '', 0.9),
                optionsTitle: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }),
                backToTree: new Button(75, 585, '', 0x007bff, 0xFFFFFF, true)
            }
        };


        this.pages.submission.submissionTitle.anchor.set(0.5);
        this.pages.submission.submissionTitle.x = 300;
        this.pages.submission.submissionTitle.y = 100;
        this.pages.submission.btnDelete.setOnClick(this.handleDeleteExercice.bind(this));
        this.pages.submission.btnTest.setOnClick(this.handleTestExercice.bind(this));
        this.pages.submission.btnSubmit.setOnClick(this.handleSubmitExercice.bind(this));
        this.pages.options.optionsTitle.anchor.set(0.5);
        this.pages.options.optionsTitle.x = 300;
        this.pages.options.optionsTitle.y = 75;

        this.pages.options.backToTree.setOnClick(this.showTree.bind(this));
        this.pages.submission.backToName.setOnClick(this.displayName.bind(this));

        this.pages.name.backToOptions.setOnClick(this.showOptions.bind(this));

        this.pages.name.fr.anchor.set(0.5);
        this.pages.name.fr.x = 300;
        this.pages.name.fr.y = 200;
        this.pages.name.de.anchor.set(0.5);
        this.pages.name.de.x = 300;
        this.pages.name.de.y = 275;
        this.pages.name.en.anchor.set(0.5);
        this.pages.name.en.x = 300;
        this.pages.name.en.y = 350;
        this.pages.name.it.anchor.set(0.5);
        this.pages.name.it.x = 300;
        this.pages.name.it.y = 425;

        this.elements.push(this.tree,
            this.mainTitle,
            this.startButton,
            this.endButton,
            this.pages.submission.btnSubmit,
            this.pages.submission.btnDelete,
            this.pages.submission.btnTest,
            this.pages.submission.submissionTitle,
            this.nextBtn,
            this.pages.options.showLastName,
            this.pages.options.showFirstName,
            this.pages.options.showAge,
            this.pages.options.showBirthdate,
            this.pages.options.showDeathdate,
            this.pages.options.showParents,
            this.pages.options.showAddress,
            this.pages.options.showChild,
            this.pages.options.optionsTitle,
            this.pages.options.backToTree,
            this.pages.submission.backToName,
            this.addIntruderBtn,
            this.nameButton,
            this.editNameButton,
            this.pages.name.backToOptions,
            this.pages.name.fr,
            this.pages.name.en,
            this.pages.name.it,
            this.pages.name.de
        );
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show(isFromTrainMode = false) {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        this.clear();
        if (isFromTrainMode) {
            this.endInsertion();
        } else {
            this.showTitle();
            this.reset();
            this.generateTree(this.refGame.global.resources.getDegre());
        }
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        if (this.currentTutorialText && this.currentTutorialText !== '')
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentTutorialText));
        this.startButton.setText(this.refGame.global.resources.getOtherText('startMode'));
        this.mainTitle.text = this.refGame.global.resources.getOtherText('createModeTitle');
        this.endButton.setText(this.refGame.global.resources.getOtherText('endButton'));
        this.nextBtn.setText(this.refGame.global.resources.getOtherText('nextBtn'));
        this.pages.submission.btnSubmit.setText(this.refGame.global.resources.getOtherText('submit'));
        this.pages.submission.btnDelete.setText(this.refGame.global.resources.getOtherText('delete'));
        this.pages.submission.btnTest.setText(this.refGame.global.resources.getOtherText('test'));
        this.pages.submission.submissionTitle.text = this.refGame.global.resources.getOtherText('submissionTitle');

        this.pages.options.showLastName.setText(this.refGame.global.resources.getOtherText('showLastName'));
        this.pages.options.showFirstName.setText(this.refGame.global.resources.getOtherText('showFirstName'));
        this.pages.options.showAge.setText(this.refGame.global.resources.getOtherText('showAge'));
        this.pages.options.showBirthdate.setText(this.refGame.global.resources.getOtherText('showBirthdate'));
        this.pages.options.showDeathdate.setText(this.refGame.global.resources.getOtherText('showDeathdate'));
        this.pages.options.showParents.setText(this.refGame.global.resources.getOtherText('showParents'));
        this.pages.options.showAddress.setText(this.refGame.global.resources.getOtherText('showAddress'));
        this.pages.options.showChild.setText(this.refGame.global.resources.getOtherText('showChild'));
        this.pages.options.optionsTitle.text = this.refGame.global.resources.getOtherText('optionsTitle');

        this.pages.submission.backToName.setText(this.refGame.global.resources.getOtherText('back'));
        this.pages.options.backToTree.setText(this.refGame.global.resources.getOtherText('back'));
        this.addIntruderBtn.setText(this.refGame.global.resources.getOtherText('addIntruder'));
        this.editNameButton.setText(this.refGame.global.resources.getOtherText('editName'));
        this.nameButton.setText(this.refGame.global.resources.getOtherText('nextBtn'));
        this.pages.name.backToOptions.setText(this.refGame.global.resources.getOtherText('back'));

        this.pages.name.fr.text = this.names.fr && this.names.fr !== '' ? this.names.fr : this.refGame.global.resources.getOtherText('noNameFor', {
            lang: this.refGame.global.resources.getOtherText('french')
        });
        this.pages.name.de.text = this.names.de && this.names.de !== '' ? this.names.de : this.refGame.global.resources.getOtherText('noNameFor', {
            lang: this.refGame.global.resources.getOtherText('german')
        });
        this.pages.name.en.text = this.names.en && this.names.en !== '' ? this.names.en : this.refGame.global.resources.getOtherText('noNameFor', {
            lang: this.refGame.global.resources.getOtherText('english')
        });
        this.pages.name.it.text = this.names.it && this.names.it !== '' ? this.names.it : this.refGame.global.resources.getOtherText('noNameFor', {
            lang: this.refGame.global.resources.getOtherText('italien')
        });

    }

    reset() {
        this.currentTutorialText = "";
        this.refGame.global.util.setTutorialText('');
        this.pages.options.showLastName.select(this.pages.options.showLastName.isChecked());
        this.pages.options.showFirstName.select(this.pages.options.showFirstName.isChecked());
        this.pages.options.showAge.select(this.pages.options.showAge.isChecked());
        this.pages.options.showBirthdate.select(this.pages.options.showBirthdate.isChecked());
        this.pages.options.showDeathdate.select(this.pages.options.showDeathdate.isChecked());
        this.pages.options.showParents.select(this.pages.options.showParents.isChecked());
        this.pages.options.showAddress.select(this.pages.options.showAddress.isChecked());
        this.pages.options.showChild.select(this.pages.options.showChild.isChecked());
        this.tree.reset();
        this.options = {};
        this.intruders = [];
        this.createdCharacter = [];
        this.characters = {
            me: ['c_m_1', 'i_m_2'],
            parents: {
                men: ['i_m_1', 'a_m_1'],
                women: ['a_w_1']
            }, grandParents: {
                men: ['o_m_1', 'o_m_2', 'o_m_3', 'o_m_4', 'o_m_5', 'o_m_6', 'i_m_3'],
                women: ['o_w_1', 'o_w_2', 'o_w_3', 'o_w_4', 'o_w_5', 'o_w_6', 'i_w_1']
            }
        };
        this.intrudersList = [
            'o_w_1',
            'o_w_2',
            'o_w_3',
            'o_w_4',
            'o_w_5',
            'o_w_6',
            'i_w_1',
            'o_m_1',
            'o_m_2',
            'o_m_3',
            'o_m_4',
            'o_m_5',
            'o_m_6',
            'i_m_3',
            'i_m_1',
            'a_m_1',
            'a_w_1',
            'c_m_1',
            'i_m_2'
        ];
    }

    showTree() {
        this.hideAll();
        this.tree.setVisible(true);
        this.nextBtn.setVisible(true);
        this.addIntruderBtn.setVisible(true);
        this.currentTutorialText = "create";
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentTutorialText));
    }


    showTitle() {
        this.hideAll();
        this.mainTitle.visible = true;
        this.startButton.setVisible(true);
    }

    generateTree(degree) {
        this.createdCharacter.push([{}]);
        this.createdCharacter.push([{}, {}]);
        this.createdCharacter.push([{}, {}, {}, {}]);

        if (degree > 3) {
            this.createdCharacter.push([{}, {}, {}, {}, {}, {}, {}, {}]);
            this.tree.addEmptyCell(50, 100, 50, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
                gender: 'women',
                age: 'greatGrandParents',
                tablePosX: 0,
                tablePosY: 3
            });
            this.tree.addEmptyCell(105, 100, 50, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
                gender: 'men',
                age: 'greatGrandParents',
                tablePosX: 1,
                tablePosY: 3
            });

            this.tree.addEmptyCell(185, 100, 50, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
                gender: 'women',
                age: 'greatGrandParents',
                tablePosX: 2,
                tablePosY: 3
            });
            this.tree.addEmptyCell(240, 100, 50, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
                gender: 'men',
                age: 'greatGrandParents',
                tablePosX: 3,
                tablePosY: 3
            });

            this.tree.addEmptyCell(360, 100, 50, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
                gender: 'women',
                age: 'greatGrandParents',
                tablePosX: 4,
                tablePosY: 3
            });
            this.tree.addEmptyCell(415, 100, 50, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
                gender: 'men',
                age: 'greatGrandParents',
                tablePosX: 5,
                tablePosY: 3
            });

            this.tree.addEmptyCell(495, 100, 50, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
                gender: 'women',
                age: 'greatGrandParents',
                tablePosX: 6,
                tablePosY: 3
            });
            this.tree.addEmptyCell(550, 100, 50, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
                gender: 'men',
                age: 'greatGrandParents',
                tablePosX: 7,
                tablePosY: 3
            });

        }

        //Second line
        this.tree.addEmptyCell(degree > 3 ? 118 : 113, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
            gender: 'women',
            age: 'grandParents',
            tablePosX: 0,
            tablePosY: 2
        });
        this.tree.addEmptyCell(degree > 3 ? 173 : 178, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
            gender: 'men',
            age: 'grandParents',
            tablePosX: 1,
            tablePosY: 2
        });

        this.tree.addEmptyCell(degree > 3 ? 427 : 422, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
            gender: 'women',
            age: 'grandParents',
            tablePosX: 2,
            tablePosY: 2
        });
        this.tree.addEmptyCell(degree > 3 ? 482 : 487, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
            gender: 'men',
            age: 'grandParents',
            tablePosX: 3,
            tablePosY: 2
        });

        //Third line
        this.tree.addEmptyCell(degree > 3 ? 230 : 225, degree > 3 ? 300 : 250, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
            gender: 'women',
            age: 'parents',
            tablePosX: 0,
            tablePosY: 1
        });
        this.tree.addEmptyCell(degree > 3 ? 370 : 375, degree > 3 ? 300 : 250, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
            gender: 'men',
            age: 'parents',
            tablePosX: 1,
            tablePosY: 1
        });

        //Fourth line
        this.tree.addEmptyCell(300, 400, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cell'), this.onCellClick.bind(this), {
            age: 'me',
            tablePosX: 0,
            tablePosY: 0
        });
    }

    onCellClick(cell) {
        this.askForCharacter(cell, null, false);
    }

    askForCharacter(cell, infos) {
        infos = infos
            ? infos
            : (this.createdCharacter[cell.cellInfo.tablePosY][cell.cellInfo.tablePosX].birthdate
                ? this.createdCharacter[cell.cellInfo.tablePosY][cell.cellInfo.tablePosX]
                : undefined);

        Swal.fire({
            title: this.refGame.global.resources.getOtherText('addCharacter'),
            width: 1000,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK',
            cancelButtonText: this.refGame.global.resources.getOtherText('cancel'),
            html:
                '<div style="text-align: left">' +
                '<form>' +
                '  <div class="row">' +
                '<div class="col-6"> ' +
                '<div class="form-group row">' +
                '    <label for="firstname" class="col-3 col-form-label">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="firstname" name="firstname" type="text" required="required" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="lastname" class="col-3 col-form-label">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="lastname" name="lastname" value="" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6">  ' +
                '<div class="form-group row">' +
                '    <label for="birthdate" class="col-5 col-form-label">' + this.refGame.global.resources.getOtherText('birthdate', {param: ''}) + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="birthdate" name="bithdate" type="date" value="" class="form-control"> ' +
                '        <div class="input-group-append">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('age', {param: ''}) + '&nbsp;' + '<span id="age"></span></div>' +
                '        </div>' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="deathdate" class="col-5 col-form-label">' + this.refGame.global.resources.getOtherText('deathdate', {param: ''}) + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="deathdate" name="deathdate" type="date" value="" class="form-control"> ' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '  <div class="form-group row">' +
                '    <label for="address" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('address', {param: ''}) + '</label> ' +
                '    <div class="col-10">' +
                '      <textarea id="address" name="address" cols="40" rows="2"  value="" class="form-control"></textarea>' +
                '    </div>' +
                '  </div>' +
                ((cell.cellInfo.age === 'greatGrandParents' || (this.refGame.global.resources.getDegre() < 4 && cell.cellInfo.age === 'grandParents')) ?
                        ('  <div class="form-group row">' +
                            '    <label for="motherLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('mother') + '</label> ' +
                            '    <div class="col-10">' +
                            '      <div class="input-group">' +
                            '        <div class="input-group-prepend">' +
                            '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                            '        </div> ' +
                            '        <input id="motherLastName" name="motherLastName" placeholder="" type="text" value="" class="form-control">' +
                            '        <div class="input-group-prepend">' +
                            '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                            '        </div> ' +
                            '        <input id="motherFirstName" name="motherFirstName" placeholder="" type="text"  value="" class="form-control">' +
                            '      </div>' +
                            '    </div>' +
                            '  </div>' +
                            '  <div class="form-group row">' +
                            '    <label for="fatherLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('father') + '</label> ' +
                            '    <div class="col-10">' +
                            '      <div class="input-group">' +
                            '        <div class="input-group-prepend">' +
                            '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                            '        </div> ' +
                            '        <input id="fatherLastName" name="fatherLastName" placeholder="" type="text" class="form-control" value="">' +
                            '        <div class="input-group-prepend">' +
                            '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                            '        </div> ' +
                            '        <input id="fatherFirstName" name="fatherFirstName" placeholder="" type="text" class="form-control" value="">' +
                            '      </div>' +
                            '    </div>' +
                            '  </div>')
                        : ''
                ) +
                '  <div class="form-group row">' +
                '    <label for="text" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('character') + '</label> ' +
                '    <div class="col-10">' +
                '       <div id="selector"></div>' +
                '    </div>' +
                '  </div>' +
                ' ' +
                '</form>' +
                '<input type="hidden" id="character"><br><br>' +
                '</div>',
            onBeforeOpen: function () {

                document.getElementById('birthdate').onchange = function () {
                    document.getElementById('age').innerHTML = this.calculateAge(document.getElementById('birthdate').value, document.getElementById('deathdate').value);
                }.bind(this);
                document.getElementById('deathdate').onchange = function () {
                    document.getElementById('age').innerHTML = this.calculateAge(document.getElementById('birthdate').value, document.getElementById('deathdate').value);
                }.bind(this);


                let icons = [];
                let gender = cell.cellInfo.gender;
                let age = cell.cellInfo.age;
                age = age === 'greatGrandParents' ? 'grandParents' : age;
                if (gender) {
                    for (let char of this.characters[age][gender]) {
                        for (let intruder of this.intruders) {
                            if (intruder.getInfo().image === char)
                                continue;
                        }
                        icons.push({
                            iconFilePath: this.refGame.global.resources.getImage(char).image.src,
                            iconValue: char
                        });
                    }
                } else {
                    for (let char of this.characters[age]) {
                        for (let intruder of this.intruders) {
                            if (intruder.getInfo().image === char)
                                continue;
                        }
                        icons.push({
                            iconFilePath: this.refGame.global.resources.getImage(char).image.src,
                            iconValue: char
                        });
                    }
                }
                if (infos && infos.image) {
                    icons.splice(0, 0, {
                        iconFilePath: this.refGame.global.resources.getImage(infos.image).image.src,
                        iconValue: infos.image
                    });
                }

                let select = new IconSelect("selector", {
                    'selectedIconWidth': 'auto',
                    'selectedIconHeight': 70,
                    'selectedBoxPadding': 5,
                    'iconsWidth': 'auto',
                    'iconsHeight': 70,
                    'boxWidth': 32,
                    'boxHeight': 75,
                    'boxIconSpace': 3,
                    'vectoralIconNumber': (icons.length <= 15 ? icons.length : 15),
                    'horizontalIconNumber': (icons.length <= 15 ? 1 : (Math.floor(icons.length / 15) + 1))
                });
                select.refresh(icons);

                if (infos && infos.image) {
                    select.setSelectedValue(infos.image);
                }

                document.getElementById('selector').addEventListener('changed', function (e) {
                    document.getElementById('character').value = select.getSelectedValue();
                });

                if (infos) {
                    $('#character').val(infos.image);
                    $('#firstname').val(infos.firstName);
                    $('#lastname').val(infos.lastName);
                    $('#birthdate').val(this.stringToDate(infos.birthdate));
                    $('#deathdate').val(this.stringToDate(infos.deathdate));
                    $('#address').val(infos.address.replace(", ", "\n"));
                    if ($('#fatherFirstName').length) $('#fatherFirstName').val(infos.father.firstName);
                    if ($('#fatherLastName').length) $('#fatherLastName').val(infos.father.lastName);
                    if ($('#motherFirstName').length) $('#motherFirstName').val(infos.mother.firstName);
                    if ($('#motherLastName').length) $('#motherLastName').val(infos.mother.lastName);
                }

            }.bind(this),
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#character').val(),
                        $('#firstname').val(),
                        $('#lastname').val(),
                        $('#birthdate').val(),
                        $('#deathdate').val(),
                        $('#address').val(),
                        $('#fatherFirstName').length ? $('#fatherFirstName').val() : '',
                        $('#fatherLastName').length ? $('#fatherLastName').val() : '',
                        $('#motherFirstName').length ? $('#motherFirstName').val() : '',
                        $('#motherLastName').length ? $('#motherLastName').val() : '',
                    ])
                })
            }
        }).then(function (result) {
            if (result && result.value && result.value.length === 10) {
                let character = {
                    lastName: result.value[2].trim(),
                    firstName: result.value[1].trim(),
                    birthdate: this.dateToString(result.value[3].trim()),
                    deathdate: this.dateToString(result.value[4].trim()),
                    address: result.value[5].replace("\n", ", ").trim(),
                    image: result.value[0].trim(),
                    mother: {
                        firstName: result.value[8].trim(),
                        lastName: result.value[9].trim()
                    },
                    father: {
                        firstName: result.value[6].trim(),
                        lastName: result.value[7].trim()
                    },
                    child: {}
                };
                let checkResult = this.checkForError(cell, character, false);
                if (checkResult.code === 'OK') {
                    cell.setSprite(this.refGame.global.resources.getImage(character.image), character, this.askForCharacter.bind(this));
                    cell.onClick = undefined;
                    this.removeCharacterFromList(infos, character.image, cell.cellInfo.gender, cell.cellInfo.age);
                    this.createdCharacter[cell.cellInfo.tablePosY][cell.cellInfo.tablePosX] = character;
                    this.tree.display();
                } else {
                    Swal.fire(
                        this.refGame.global.resources.getOtherText('error'),
                        checkResult.error, "error").then(function () {
                        this.askForCharacter(cell, character);
                    }.bind(this));
                }
            }
        }.bind(this));
    }

    removeCharacterFromList(infos, image, gender, age) {
        this.intrudersList.splice(this.intrudersList.indexOf(image), 1);
        age = age === 'greatGrandParents' ? 'grandParents' : age;
        if (gender) {
            this.characters[age][gender].splice(this.characters[age][gender].indexOf(image), 1);
        } else {
            this.characters[age].splice(this.characters[age].indexOf(image), 1);
        }
        //Add old image to array
        if (infos && infos.image !== image) {
            this.intrudersList.push(infos.image);
            if (gender) {
                this.characters[age][gender].splice(0, 0, infos.image);
            } else {
                this.characters[age].splice(0, 0, infos.image);
            }
        }
    }

    checkForError(cell, character, isIntruder = false) {
        let displayParent = isIntruder ? true : (cell.cellInfo.age === 'greatGrandParents' || (this.refGame.global.resources.getDegre() < 4 && cell.cellInfo.age === 'grandParents'));

        let checkResult = {
            code: 'OK',
            error: ''
        };
        if (!(character.lastName && character.lastName !== ''
            && character.firstName && character.firstName !== ''
            && character.birthdate && character.birthdate !== ''
            && character.address && character.address !== ''
            && character.image && character.image !== ''
            && (!displayParent ^ (character.mother.firstName && character.mother.firstName !== ''
                && character.mother.lastName && character.mother.lastName !== ''
                && character.father.firstName && character.father.firstName !== ''
                && character.father.lastName && character.father.lastName !== ''))
            && !(isIntruder ^ (character.child.firstName && character.child.firstName !== ''
                && character.child.lastName && character.child.lastName !== '')))) {
            checkResult.code = 'error';
            checkResult.error = 'informationsMissing';
        } else {
            if (!displayParent) {
                let father = this.createdCharacter[cell.cellInfo.tablePosY + 1][cell.cellInfo.tablePosX * 2 + 1];
                let mother = this.createdCharacter[cell.cellInfo.tablePosY + 1][cell.cellInfo.tablePosX * 2];
                //test mother
                if (mother.birthdate && (this.calculateAge(mother.birthdate, '') < this.calculateAge(character.birthdate, '') + 10)) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooOldForMother';
                }
                //test father
                else if (father.birthdate && (this.calculateAge(father.birthdate, '') < this.calculateAge(character.birthdate, '') + 10)) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooOldForFather';
                }
            }
            if (!isIntruder && cell.cellInfo.tablePosY > 0) {
                let child = this.createdCharacter[cell.cellInfo.tablePosY - 1][Math.floor(cell.cellInfo.tablePosX / 2)];
                if (child.birthdate && (this.calculateAge(child.birthdate, '') > this.calculateAge(character.birthdate, '') - 10)) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooYoungForChild';
                }
            }
        }
        checkResult.error = checkResult.code !== 'OK' ? this.refGame.global.resources.getOtherText(checkResult.error) : '';
        return checkResult;
    }

    calculateAge(birthDateString, deathDateString) {
        birthDateString = birthDateString.split('.').reverse().join('-');
        let birthdate = new Date(birthDateString);
        let age;
        let month;
        if (deathDateString && deathDateString !== "") {
            deathDateString = deathDateString.split('.').reverse().join('-');
            let deathdate = new Date(deathDateString);
            age = deathdate.getFullYear() - birthdate.getFullYear();
            month = deathdate.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && deathdate.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return (age < 0 || isNaN(age)) ? 0 : age;
        } else {
            let today = new Date();
            age = today.getFullYear() - birthdate.getFullYear();
            month = today.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return (age < 0 || isNaN(age)) ? 0 : age;
        }
    }

    dateToString(date) {
        if (!date || date === '')
            return '';
        return date.split('-').reverse().join('.');
    }

    stringToDate(string) {
        if (!string || string === '')
            return '';
        return string.split('.').reverse().join('-');
    }

    endInsertion() {
        if (this.names.fr && this.names.fr !== '' && this.names.de && this.names.de !== '') {
            this.hideAll();
            this.pages.submission.btnSubmit.setVisible(true);
            this.pages.submission.btnDelete.setVisible(true);
            this.pages.submission.btnTest.setVisible(true);
            this.pages.submission.submissionTitle.visible = true;
            this.pages.submission.backToName.setVisible(true);
        } else {
            Swal.fire(
                this.refGame.global.resources.getOtherText('warning'),
                this.refGame.global.resources.getOtherText('noTranslation'),
                'warning');
        }
    }

    handleDeleteExercice() {
        Swal.fire({
            title: this.refGame.global.resources.getOtherText('confirmDeletion'),
            text: this.refGame.global.resources.getOtherText('confirmDeletionText'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33d33',
            confirmButtonText: this.refGame.global.resources.getOtherText('yes'),
            cancelButtonText: this.refGame.global.resources.getOtherText('no')
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    this.refGame.global.resources.getOtherText('deleted'),
                    this.refGame.global.resources.getOtherText('deletedExerciceText'),
                    'success'
                );
                this.show();
            }
        });
    }

    handleTestExercice() {
        let exercice = this.buildExercice();
        this.refGame.trainMode.init(false, 'createMode');
        this.refGame.trainMode.show(exercice);
    }

    handleSubmitExercice() {
        let exercice = this.buildExercice();
        console.log(exercice);
        console.log(this.intruders)
        this.refGame.global.resources.saveExercice(JSON.stringify(exercice), function (result) {
            Swal.fire(
                result.message,
                undefined,
                result.type
            );
            this.show();
        }.bind(this));
    }

    buildExercice() {
        let exercice = {
            scenario: {},
            options: this.options,
            name: this.names
        };
        exercice.scenario.intrus = [];
        exercice.options.numberOfIntruder = this.intruders.length;

        for (let intruder of this.intruders) {
            let intruderInfo = intruder.getInfo();
            exercice.scenario.intrus.push(intruderInfo);
        }

        //Level 1
        exercice.scenario.enfant = this.createdCharacter[0][0];

        //Level 2
        exercice.scenario.enfant.mother = this.createdCharacter[1][0];
        exercice.scenario.enfant.father = this.createdCharacter[1][1];

        //Level 3
        exercice.scenario.enfant.mother.mother = this.createdCharacter[2][0];
        exercice.scenario.enfant.mother.father = this.createdCharacter[2][1];
        exercice.scenario.enfant.father.mother = this.createdCharacter[2][2];
        exercice.scenario.enfant.father.father = this.createdCharacter[2][3];

        //Level 4 if exists
        if (this.refGame.global.resources.getDegre() > 3) {
            exercice.scenario.enfant.mother.mother.mother = this.createdCharacter[3][0];
            exercice.scenario.enfant.mother.mother.father = this.createdCharacter[3][1];
            exercice.scenario.enfant.mother.father.mother = this.createdCharacter[3][2];
            exercice.scenario.enfant.mother.father.father = this.createdCharacter[3][3];
            exercice.scenario.enfant.father.mother.mother = this.createdCharacter[3][4];
            exercice.scenario.enfant.father.mother.father = this.createdCharacter[3][5];
            exercice.scenario.enfant.father.father.mother = this.createdCharacter[3][6];
            exercice.scenario.enfant.father.father.father = this.createdCharacter[3][7];
        }

        return exercice;
    }

    showOptions() {
        let total = 0, created = 0;
        for (let level of this.createdCharacter) {
            for (let character of level) {
                total++;
                if (character.birthdate)
                    created++;
            }
        }
        if (created < total) {
            Swal.fire(
                this.refGame.global.resources.getOtherText('warning'),
                this.refGame.global.resources.getOtherText('notFull'),
                'warning');
        } else {
            this.currentTutorialText = "options";
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentTutorialText));
            this.hideAll();
            this.nameButton.setVisible(true);
            this.pages.options.optionsTitle.visible = true;
            this.pages.options.showLastName.setVisible(true);
            this.pages.options.showFirstName.setVisible(true);
            this.pages.options.showAge.setVisible(true);
            this.pages.options.showBirthdate.setVisible(true);
            this.pages.options.showDeathdate.setVisible(true);
            this.pages.options.showParents.setVisible(true);
            this.pages.options.showAddress.setVisible(true);
            this.pages.options.showChild.setVisible(true);
            this.pages.options.backToTree.setVisible(true);
        }
    }


    hideAll() {
        this.nextBtn.setVisible(false);
        this.endButton.setVisible(false);
        this.tree.setVisible(false);
        this.mainTitle.visible = false;
        this.startButton.setVisible(false);
        this.pages.submission.btnSubmit.setVisible(false);
        this.pages.submission.btnDelete.setVisible(false);
        this.pages.submission.btnTest.setVisible(false);
        this.pages.submission.submissionTitle.visible = false;
        this.pages.submission.backToName.setVisible(false);
        this.pages.options.optionsTitle.visible = false;
        this.pages.options.showLastName.setVisible(false);
        this.pages.options.showFirstName.setVisible(false);
        this.pages.options.showAge.setVisible(false);
        this.pages.options.showBirthdate.setVisible(false);
        this.pages.options.showDeathdate.setVisible(false);
        this.pages.options.showParents.setVisible(false);
        this.pages.options.showAddress.setVisible(false);
        this.pages.options.showChild.setVisible(false);
        this.pages.options.backToTree.setVisible(false);
        this.addIntruderBtn.setVisible(false);
        this.editNameButton.setVisible(false);
        this.nameButton.setVisible(false);
        this.pages.name.fr.visible = false;
        this.pages.name.de.visible = false;
        this.pages.name.en.visible = false;
        this.pages.name.it.visible = false;
        this.pages.name.backToOptions.setVisible(false);

    }


    addIntruder(character) {
        let total = 0, created = 0;
        for (let level of this.createdCharacter) {
            for (let character of level) {
                total++;
                if (character.birthdate)
                    created++;
            }
        }
        if (total !== created) {
            Swal.fire(
                this.refGame.global.resources.getOtherText('warning'),
                this.refGame.global.resources.getOtherText('notFull'),
                'warning');
        } else
            this.askForIntruder(character, character ? character.getInfo() : undefined);
    }

    askForIntruder(cell, infos) {
        Swal.fire({
            title: this.refGame.global.resources.getOtherText('addCharacter'),
            width: 1000,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK',
            cancelButtonText: this.refGame.global.resources.getOtherText('cancel'),
            html:
                '<div style="text-align: left">' +
                '<form>' +
                '  <div class="row">' +
                '<div class="col-6"> ' +
                '<div class="form-group row">' +
                '    <label for="firstname" class="col-3 col-form-label">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="firstname" name="firstname" type="text" required="required" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="lastname" class="col-3 col-form-label">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="lastname" name="lastname" value="" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6">  ' +
                '<div class="form-group row">' +
                '    <label for="birthdate" class="col-5 col-form-label">' + this.refGame.global.resources.getOtherText('birthdate', {param: ''}) + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="birthdate" name="bithdate" type="date" value="" class="form-control"> ' +
                '        <div class="input-group-append">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('age', {param: ''}) + '&nbsp;' + '<span id="age"></span></div>' +
                '        </div>' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="deathdate" class="col-5 col-form-label">' + this.refGame.global.resources.getOtherText('deathdate', {param: ''}) + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="deathdate" name="deathdate" type="date" value="" class="form-control"> ' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '  <div class="form-group row">' +
                '    <label for="address" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('address', {param: ''}) + '</label> ' +
                '    <div class="col-10">' +
                '      <textarea id="address" name="address" cols="40" rows="2"  value="" class="form-control"></textarea>' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="motherLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('mother') + '</label> ' +
                '    <div class="col-10">' +
                '      <div class="input-group">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="motherLastName" name="motherLastName" placeholder="" type="text" value="" class="form-control">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="motherFirstName" name="motherFirstName" placeholder="" type="text"  value="" class="form-control">' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="fatherLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('father') + '</label> ' +
                '    <div class="col-10">' +
                '      <div class="input-group">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="fatherLastName" name="fatherLastName" placeholder="" type="text" class="form-control" value="">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="fatherFirstName" name="fatherFirstName" placeholder="" type="text" class="form-control" value="">' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="childLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('child',{param: ''}) + '</label> ' +
                '    <div class="col-10">' +
                '      <div class="input-group">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="childLastName" name="childLastName" placeholder="" type="text" class="form-control" value="">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="childFirstName" name="childFirstName" placeholder="" type="text" class="form-control" value="">' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="text" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('character') + '</label> ' +
                '    <div class="col-10">' +
                '       <div id="selector"></div>' +
                '    </div>' +
                '  </div>' +
                '</form>' +
                '<input type="hidden" id="character"><br><br>' +
                '</div>',
            onBeforeOpen: function () {

                document.getElementById('birthdate').onchange = function () {
                    document.getElementById('age').innerHTML = this.calculateAge(document.getElementById('birthdate').value, document.getElementById('deathdate').value);
                }.bind(this);
                document.getElementById('deathdate').onchange = function () {
                    document.getElementById('age').innerHTML = this.calculateAge(document.getElementById('birthdate').value, document.getElementById('deathdate').value);
                }.bind(this);

                let icons = [];
                for (let char of this.intrudersList) {
                    icons.push({
                        iconFilePath: this.refGame.global.resources.getImage(char).image.src,
                        iconValue: char
                    });
                }
                if (infos && infos.image) {
                    icons.push({
                        iconFilePath: this.refGame.global.resources.getImage(infos.image).image.src,
                        iconValue: infos.image
                    });
                }

                let select = new IconSelect("selector", {
                    'selectedIconWidth': 'auto',
                    'selectedIconHeight': 70,
                    'selectedBoxPadding': 5,
                    'iconsWidth': 'auto',
                    'iconsHeight': 70,
                    'boxWidth': 32,
                    'boxHeight': 75,
                    'boxIconSpace': 3,
                    'vectoralIconNumber': (icons.length <= 15 ? icons.length : 15),
                    'horizontalIconNumber': (icons.length <= 15 ? 1 : (Math.floor(icons.length / 15) + 1))
                });

                select.refresh(icons);

                if (infos && infos.image) {
                    select.setSelectedValue(infos.image);
                }

                document.getElementById('selector').addEventListener('changed', function (e) {
                    document.getElementById('character').value = select.getSelectedValue();
                });

                if (infos) {
                    $('#character').val(infos.image);
                    $('#firstname').val(infos.firstName);
                    $('#lastname').val(infos.lastName);
                    $('#birthdate').val(this.stringToDate(infos.birthdate));
                    $('#deathdate').val(this.stringToDate(infos.deathdate));
                    $('#address').val(infos.address.replace(", ", "\n"));
                    if ($('#fatherFirstName').length) $('#fatherFirstName').val(infos.father.firstName);
                    if ($('#fatherLastName').length) $('#fatherLastName').val(infos.father.lastName);
                    if ($('#motherFirstName').length) $('#motherFirstName').val(infos.mother.firstName);
                    if ($('#motherLastName').length) $('#motherLastName').val(infos.mother.lastName);
                    if ($('#childFirstName').length) $('#childFirstName').val(infos.child.firstName);
                    if ($('#childLastName').length) $('#childLastName').val(infos.child.lastName);
                }

            }.bind(this),
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#character').val(),
                        $('#firstname').val(),
                        $('#lastname').val(),
                        $('#birthdate').val(),
                        $('#deathdate').val(),
                        $('#address').val(),
                        $('#fatherFirstName').length ? $('#fatherFirstName').val() : '',
                        $('#fatherLastName').length ? $('#fatherLastName').val() : '',
                        $('#motherFirstName').length ? $('#motherFirstName').val() : '',
                        $('#motherLastName').length ? $('#motherLastName').val() : '',
                        $('#childFirstName').length ? $('#childFirstName').val() : '',
                        $('#childLastName').length ? $('#childLastName').val() : '',
                    ])
                })
            }
        }).then(function (result) {
            if (result && result.value && result.value.length === 12) {
                let character = {
                    lastName: result.value[2].trim(),
                    firstName: result.value[1].trim(),
                    birthdate: this.dateToString(result.value[3].trim()),
                    deathdate: this.dateToString(result.value[4].trim()),
                    address: result.value[5].replace("\n", ", ").trim(),
                    image: result.value[0].trim(),
                    mother: {
                        firstName: result.value[8].trim(),
                        lastName: result.value[9].trim()
                    },
                    father: {
                        firstName: result.value[6].trim(),
                        lastName: result.value[7].trim()
                    },
                    child: {
                        firstName: result.value[10].trim(),
                        lastName: result.value[11].trim()
                    }
                };
                let checkResult = this.checkForError(cell, character, true);
                if (checkResult.code === 'OK') {
                    this.tree.intruders = [];
                    if (this.intruders.indexOf(cell) > -1) {
                        this.intruders.slice(this.intruders.indexOf(cell), 1);
                        cell = this.intruders[this.intruders.indexOf(cell)];
                        cell = new Character(30 + cell.getX(), cell.getY(), 22, 65, this.refGame.global.resources.getImage(character.image), character, undefined, this.addIntruder.bind(this));
                        cell.setCanMove(false);
                        this.intruders[this.intruders.indexOf(cell)] = cell;
                        if (character.image !== infos.image) {
                            this.intrudersList.push(infos.image);
                        }
                    } else {
                        let x = 20;
                        let y = 530;
                        if (this.intruders.length > 0) {
                            x = this.intruders[this.intruders.length - 1].getX();
                        }
                        let char = new Character(30 + x, y, 22, 65, this.refGame.global.resources.getImage(character.image), character, undefined, this.addIntruder.bind(this));
                        char.setCanMove(false);
                        this.intruders.push(char);
                    }
                    for (let char of this.intruders) {
                        this.tree.addCharacter(char, true);
                    }
                    this.tree.display();
                    this.intrudersList.splice(this.intrudersList.indexOf(character.image), 1);
                    console.log("OKKKKKKKKKK");
                } else {
                    Swal.fire(
                        this.refGame.global.resources.getOtherText('error'),
                        checkResult.error, "error").then(function () {
                        this.askForIntruder(cell, character);
                    }.bind(this));
                }
            }
        }.bind(this));
    }

    editName() {

        Swal.fire({
            title: this.refGame.global.resources.getOtherText('chooseName'),
            html: '<form>' +
                '  <div class="form-group row">' +
                '    <label for="fr" class="col-4 col-form-label">' + this.refGame.global.resources.getOtherText('french') + '</label> ' +
                '    <div class="col-8">' +
                '      <input id="frName" name="fr" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="de" class="col-4 col-form-label">' + this.refGame.global.resources.getOtherText('german') + '</label> ' +
                '    <div class="col-8">' +
                '      <input id="deName" name="de" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="en" class="col-4 col-form-label">' + this.refGame.global.resources.getOtherText('english') + '</label> ' +
                '    <div class="col-8">' +
                '      <input id="enName" name="en" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="it" class="col-4 col-form-label">' + this.refGame.global.resources.getOtherText('italien') + '</label> ' +
                '    <div class="col-8">' +
                '      <input id="itName" name="it" type="text" class="form-control">' +
                '    </div>' +
                '  </div> ' +
                '</form>',
            confirmButtonText: 'OK',
            onBeforeOpen: function () {
                document.getElementById('frName').value = this.names.fr;
                document.getElementById('deName').value = this.names.de;
                document.getElementById('enName').value = this.names.en;
                document.getElementById('itName').value = this.names.it;
            }.bind(this),
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#frName').val(),
                        $('#deName').val(),
                        $('#enName').val(),
                        $('#itName').val()
                    ])
                })
            }
        }).then(function (result) {
            if (result && result.value && result.value.length === 4) {
                this.names.fr = result.value[0];
                this.names.de = result.value[1];
                this.names.en = result.value[2];
                this.names.it = result.value[3];
                this.pages.name.fr.text = this.names.fr && this.names.fr !== '' ? this.names.fr : this.refGame.global.resources.getOtherText('noNameFor', {
                    lang: this.refGame.global.resources.getOtherText('french')
                });
                this.pages.name.de.text = this.names.de && this.names.de !== '' ? this.names.de : this.refGame.global.resources.getOtherText('noNameFor', {
                    lang: this.refGame.global.resources.getOtherText('german')
                });
                this.pages.name.en.text = this.names.en && this.names.en !== '' ? this.names.en : this.refGame.global.resources.getOtherText('noNameFor', {
                    lang: this.refGame.global.resources.getOtherText('english')
                });
                this.pages.name.it.text = this.names.it && this.names.it !== '' ? this.names.it : this.refGame.global.resources.getOtherText('noNameFor', {
                    lang: this.refGame.global.resources.getOtherText('italien')
                });
            }
        }.bind(this));

    }

    displayName() {
        this.currentTutorialText = "name";
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentTutorialText));
        let count = 0;
        if (this.pages.options.showAddress.isChecked()) count++;
        if (this.pages.options.showAge.isChecked()) count++;
        if (this.pages.options.showBirthdate.isChecked()) count++;
        if (this.pages.options.showChild.isChecked()) count++;
        if (this.pages.options.showDeathdate.isChecked()) count++;
        if (this.pages.options.showFirstName.isChecked()) count++;
        if (this.pages.options.showLastName.isChecked()) count++;
        if (this.pages.options.showParents.isChecked()) count++;

        if (count >= 4) {
            this.hideAll();
            this.pages.name.fr.visible = true;
            this.pages.name.de.visible = true;
            this.pages.name.en.visible = true;
            this.pages.name.it.visible = true;
            this.editNameButton.setVisible(true);
            this.pages.name.backToOptions.setVisible(true);
            this.endButton.setVisible(true);
            this.options = {
                showLastName: this.pages.options.showLastName.isChecked(),
                showFirstName: this.pages.options.showFirstName.isChecked(),
                showAge: this.pages.options.showAge.isChecked(),
                showBirthdate: this.pages.options.showBirthdate.isChecked(),
                showDeathdate: this.pages.options.showDeathdate.isChecked(),
                showAddress: this.pages.options.showAddress.isChecked(),
                showParents: this.pages.options.showParents.isChecked(),
                showChild: this.pages.options.showChild.isChecked()
            }
        } else {
            Swal.fire(
                this.refGame.global.resources.getOtherText('warning'),
                this.refGame.global.resources.getOtherText('optionsNotCompleted'),
                'warning'
            );
        }
    }
}