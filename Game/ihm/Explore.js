/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */
class Explore extends Interface {


    constructor(refGame) {

        super(refGame, "explore");
        this.refGame = refGame;
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        this.clear();
        let scenario = this.refGame.global.resources.getScenario();
        this.generateTree(JSON.parse(scenario));
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('explore'));
    }

    generateTree(scenario) {
        let tree = new Tree(0, 0, 600, 600, this.refGame.global.resources.getImage('tree'));
        this.elements.push(tree);

        //SETUP CHILDREN
        scenario.enfant.mother.mother.mother.child = scenario.enfant.mother.mother.firstName + " " + scenario.enfant.mother.mother.lastName;
        scenario.enfant.mother.mother.father.child = scenario.enfant.mother.mother.firstName + " " + scenario.enfant.mother.mother.lastName;
        scenario.enfant.mother.father.mother.child = scenario.enfant.mother.father.firstName + " " + scenario.enfant.mother.father.lastName;
        scenario.enfant.mother.father.father.child = scenario.enfant.mother.father.firstName + " " + scenario.enfant.mother.father.lastName;
        scenario.enfant.father.father.mother.child = scenario.enfant.father.father.firstName + " " + scenario.enfant.father.father.lastName;
        scenario.enfant.father.father.father.child = scenario.enfant.father.father.firstName + " " + scenario.enfant.father.father.lastName;
        scenario.enfant.father.mother.mother.child = scenario.enfant.father.mother.firstName + " " + scenario.enfant.father.mother.lastName;
        scenario.enfant.father.mother.father.child = scenario.enfant.father.mother.firstName + " " + scenario.enfant.father.mother.lastName;
        scenario.enfant.mother.mother.child = scenario.enfant.mother.firstName + " " + scenario.enfant.mother.lastName;
        scenario.enfant.mother.father.child = scenario.enfant.mother.firstName + " " + scenario.enfant.mother.lastName;
        scenario.enfant.father.mother.child = scenario.enfant.father.firstName + " " + scenario.enfant.father.lastName;
        scenario.enfant.father.father.child = scenario.enfant.father.firstName + " " + scenario.enfant.father.lastName;
        scenario.enfant.mother.child = scenario.enfant.firstName + " " + scenario.enfant.lastName;
        scenario.enfant.father.child = scenario.enfant.firstName + " " + scenario.enfant.lastName;

        //First line
        tree.addCells(50, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.mother.mother.image), scenario.enfant.mother.mother.mother, this.displayInfo.bind(this));
        tree.addCells(105, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.mother.father.image), scenario.enfant.mother.mother.father, this.displayInfo.bind(this));

        tree.addCells(185, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.father.mother.image), scenario.enfant.mother.father.mother, this.displayInfo.bind(this));
        tree.addCells(240, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.father.father.image), scenario.enfant.mother.father.father, this.displayInfo.bind(this));

        tree.addCells(360, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.mother.mother.image), scenario.enfant.father.mother.mother, this.displayInfo.bind(this));
        tree.addCells(415, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.mother.father.image), scenario.enfant.father.mother.father, this.displayInfo.bind(this));

        tree.addCells(495, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.father.mother.image), scenario.enfant.father.father.mother, this.displayInfo.bind(this));
        tree.addCells(550, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.father.father.image), scenario.enfant.father.father.father, this.displayInfo.bind(this));

        //Second line
        tree.addCells(118, 200, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.mother.image), scenario.enfant.mother.mother, this.displayInfo.bind(this));
        tree.addCells(173, 200, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.father.image), scenario.enfant.mother.father, this.displayInfo.bind(this));

        tree.addCells(427, 200, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.mother.image), scenario.enfant.father.mother, this.displayInfo.bind(this));
        tree.addCells(482, 200, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.father.image), scenario.enfant.father.father, this.displayInfo.bind(this));

        //Third line
        tree.addCells(230, 300, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.image), scenario.enfant.mother, this.displayInfo.bind(this));
        tree.addCells(370, 300, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.image), scenario.enfant.father, this.displayInfo.bind(this));

        //Fourth line
        tree.addCells(300, 400, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.image), scenario.enfant, this.displayInfo.bind(this));
    }

    displayInfo(cell) {
        let personData = cell.sprite.info;
        let textToSpeech = this.refGame.global.resources.getOtherText('firstName', {param: personData.firstName}) + ";" +
            this.refGame.global.resources.getOtherText('lastName', {param: personData.lastName}) + " " + ";" +
            this.refGame.global.resources.getOtherText('age', {param: this.calculateAge(personData.birthdate, personData.deathdate)}) + " " + ";" +
            this.refGame.global.resources.getOtherText('birthdate', {param: personData.birthdate}) + " " + ";" +
            (personData.deathdate !== "" ? this.refGame.global.resources.getOtherText('deathdate', {param: personData.deathdate}) + " " : "") + ";" +
            this.refGame.global.resources.getOtherText('address', {param: personData.address}) + " " + ";" +
            this.refGame.global.resources.getOtherText('parents', {param: personData.father.firstName + " " + personData.father.lastName + " & " + personData.mother.firstName + " " + personData.mother.lastName}) + " " + ";" +
            (personData.child && personData.child !== "" ? this.refGame.global.resources.getOtherText('child', {param: personData.child}) : "");
        this.refGame.global.util.showAlertPersonnalized({
            title: personData.firstName + " " + personData.lastName,
            html: "<img src='" + this.refGame.global.resources.getImage(personData.image).getImage().src + "' style='width:20%;'><br>" +
                this.refGame.global.resources.getOtherText('firstName', {param: personData.firstName}) + "<br>" +
                this.refGame.global.resources.getOtherText('lastName', {param: personData.lastName}) + "<br>" +
                this.refGame.global.resources.getOtherText('age', {param: this.calculateAge(personData.birthdate, personData.deathdate)}) + "<br>" +
                this.refGame.global.resources.getOtherText('birthdate', {param: personData.birthdate}) + "<br>" +
                (personData.deathdate !== "" ? this.refGame.global.resources.getOtherText('deathdate', {param: personData.deathdate}) + "<br>" : "") +
                this.refGame.global.resources.getOtherText('address', {param: personData.address}) + "<br>" +
                this.refGame.global.resources.getOtherText('parents', {param: personData.father.firstName + " " + personData.father.lastName + " & " + personData.mother.firstName + " " + personData.mother.lastName}) + "<br>" +
                (personData.child && personData.child !== "" ? this.refGame.global.resources.getOtherText('child', {param: personData.child}) + "<br>" : "") +
                "<i class=\"fas fa-volume-up fa-3x\" id=\"speaker\" onclick=\"audio.textToSpeechFromTxt('" + textToSpeech + "', '" + this.refGame.global.resources.getLanguage() + "');\"></i>",
            text: ''
        });
    }

    calculateAge(birthDateString, deathDateString) {
        birthDateString = birthDateString.split('.').reverse().join('-');
        let birthdate = new Date(birthDateString);
        let age;
        let month;
        if (deathDateString !== "") {
            deathDateString = deathDateString.split('.').reverse().join('-');
            let deathdate = new Date(deathDateString);
            age = deathdate.getFullYear() - birthdate.getFullYear();
            month = deathdate.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && deathdate.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return age;
        } else {
            let today = new Date();
            age = today.getFullYear() - birthdate.getFullYear();
            month = today.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return age;
        }
    }

}
