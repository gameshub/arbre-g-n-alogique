/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */
class Train extends Interface {


    constructor(refGame) {

        super(refGame, "train");
        this.refGame = refGame;

        this.exercice = {};
        this.evaluate = false;
        this.scenario = {};
        this.options = {};
        this.tree = new Tree(0, 0, 600, 600, this.refGame.global.resources.getImage('tree'));
        this.mainTitle = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
        this.mainTitle.anchor.set(0.5);
        this.mainTitle.x = 300;
        this.mainTitle.y = 150;
        this.startButton.setOnClick(this.startGame.bind(this));
        this.elements.push(this.tree);
        this.elements.push(this.mainTitle);
        this.elements.push(this.startButton);
        this.canMove = false;
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show(evaluate, exercice = undefined, interfaceCallback = undefined) {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        this.evaluate = evaluate;
        this.exercice = exercice;
        this.interfaceCallback = interfaceCallback;
        this.clear();
        if (!this.interfaceCallback)
            this.showTitle();
        else
            this.startGame();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
        this.setupSignalement();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('train'));
        this.startButton.setText(this.refGame.global.resources.getOtherText('startMode'));
        this.mainTitle.text = this.refGame.global.resources.getOtherText(this.evaluate ? 'evalModeTitle' : 'trainModeTitle');
    }

    startGame() {
        this.canMove = true;
        if (this.evaluate)
            this.refGame.global.statistics.addStats(2);

        this.tree.setVisible(true);
        this.mainTitle.visible = false;
        this.startButton.setVisible(false);
        let exercice = {};
        if (this.exercice)
            exercice = this.exercice;
        else {
            exercice = this.refGame.global.resources.getExercice();
            this.currentExerciceId = exercice.id;
            exercice = JSON.parse(exercice.exercice);
            this.refGame.inputs.signal.style.display = 'block';
        }

        this.scenario = exercice.scenario;
        this.options = exercice.options;
        this.tree.reset();
        this.generateTree(this.refGame.global.resources.getDegre());
    }


    showTitle() {
        this.tree.setVisible(false);
        this.mainTitle.visible = true;
        this.startButton.setVisible(true);
    }

    generateTree(degree) {
        let characters = [];

        let intruders = this.scenario.intrus ? this.scenario.intrus : JSON.parse(this.refGame.global.resources.getScenario()).intrus;

        for (let intruder of intruders) {
            intruder.child = intruder.child.firstName + " " + intruder.child.lastName;
        }

        //SETUP CHILDREN
        if (degree > 3) {
            this.scenario.enfant.mother.mother.mother.child = this.scenario.enfant.mother.mother.firstName + " " + this.scenario.enfant.mother.mother.lastName;
            this.scenario.enfant.mother.mother.father.child = this.scenario.enfant.mother.mother.firstName + " " + this.scenario.enfant.mother.mother.lastName;
            this.scenario.enfant.mother.father.mother.child = this.scenario.enfant.mother.father.firstName + " " + this.scenario.enfant.mother.father.lastName;
            this.scenario.enfant.mother.father.father.child = this.scenario.enfant.mother.father.firstName + " " + this.scenario.enfant.mother.father.lastName;
            this.scenario.enfant.father.father.mother.child = this.scenario.enfant.father.father.firstName + " " + this.scenario.enfant.father.father.lastName;
            this.scenario.enfant.father.father.father.child = this.scenario.enfant.father.father.firstName + " " + this.scenario.enfant.father.father.lastName;
            this.scenario.enfant.father.mother.mother.child = this.scenario.enfant.father.mother.firstName + " " + this.scenario.enfant.father.mother.lastName;
            this.scenario.enfant.father.mother.father.child = this.scenario.enfant.father.mother.firstName + " " + this.scenario.enfant.father.mother.lastName;
        }
        this.scenario.enfant.mother.mother.child = this.scenario.enfant.mother.firstName + " " + this.scenario.enfant.mother.lastName;
        this.scenario.enfant.mother.father.child = this.scenario.enfant.mother.firstName + " " + this.scenario.enfant.mother.lastName;
        this.scenario.enfant.father.mother.child = this.scenario.enfant.father.firstName + " " + this.scenario.enfant.father.lastName;
        this.scenario.enfant.father.father.child = this.scenario.enfant.father.firstName + " " + this.scenario.enfant.father.lastName;
        this.scenario.enfant.mother.child = this.scenario.enfant.firstName + " " + this.scenario.enfant.lastName;
        this.scenario.enfant.father.child = this.scenario.enfant.firstName + " " + this.scenario.enfant.lastName;
        this.scenario.enfant.child = undefined;
        //Setup Characters
        if (degree > 3) {
            characters.push(this.scenario.enfant.mother.mother.mother);
            characters.push(this.scenario.enfant.mother.mother.father);
            characters.push(this.scenario.enfant.mother.father.mother);
            characters.push(this.scenario.enfant.mother.father.father);
            characters.push(this.scenario.enfant.father.mother.mother);
            characters.push(this.scenario.enfant.father.mother.father);
            characters.push(this.scenario.enfant.father.father.mother);
            characters.push(this.scenario.enfant.father.father.father);
        }
        characters.push(this.scenario.enfant.mother.mother);
        characters.push(this.scenario.enfant.mother.father);
        characters.push(this.scenario.enfant.father.mother);
        characters.push(this.scenario.enfant.father.father);
        characters.push(this.scenario.enfant.mother);
        characters.push(this.scenario.enfant.father);
        characters.push(this.scenario.enfant);

        let correctOrder = characters.slice(0);

        //Add intruders
        if (this.options.numberOfIntruder)
            for (let i = 0; i < this.options.numberOfIntruder; i++)
                characters.push(intruders[i]);

        this.shuffleArray(characters);
        //First line
        if (degree > 3) {
            this.tree.addCells(50, 100, 50, 0, this.refGame.global.resources.getImage('cellw'));
            this.tree.addCells(105, 100, 50, 0, this.refGame.global.resources.getImage('cellm'));

            this.tree.addCells(185, 100, 50, 0, this.refGame.global.resources.getImage('cellw'));
            this.tree.addCells(240, 100, 50, 0, this.refGame.global.resources.getImage('cellm'));

            this.tree.addCells(360, 100, 50, 0, this.refGame.global.resources.getImage('cellw'));
            this.tree.addCells(415, 100, 50, 0, this.refGame.global.resources.getImage('cellm'));

            this.tree.addCells(495, 100, 50, 0, this.refGame.global.resources.getImage('cellw'));
            this.tree.addCells(550, 100, 50, 0, this.refGame.global.resources.getImage('cellm'));
        }

        //Second line
        this.tree.addCells(degree > 3 ? 118 : 113, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'));
        this.tree.addCells(degree > 3 ? 173 : 178, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'));

        this.tree.addCells(degree > 3 ? 427 : 422, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'));
        this.tree.addCells(degree > 3 ? 482 : 487, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'));

        //Third line
        this.tree.addCells(degree > 3 ? 230 : 225, degree > 3 ? 300 : 250, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'));
        this.tree.addCells(degree > 3 ? 370 : 375, degree > 3 ? 300 : 250, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'));

        //Fourth line
        this.tree.addCells(300, 400, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cell'));

        //Add characters
        let height = degree > 3 ? 55 : 65;
        let width = (height / 65) * 22;
        for (let i = 0; i < characters.length; i++) {
            let character = new Character(15 + i * 28, 520, width, height, this.refGame.global.resources.getImage(characters[i].image), characters[i], this.checkOnMove.bind(this), this.displayInfo.bind(this));
            this.tree.addCharacter(character, intruders.indexOf(characters[i]) > -1);
        }

        this.sortCharacters(correctOrder);
    }

    displayInfo(character) {
        let personData = character.info;
        let textToSpeech = '';
        if (this.options.showFirstName)
            textToSpeech += this.refGame.global.resources.getOtherText('firstName', {param: personData.firstName}) + ";";
        if (this.options.showLastName)
            textToSpeech += this.refGame.global.resources.getOtherText('lastName', {param: personData.lastName}) + " " + ";";
        if (this.options.showAge)
            textToSpeech += this.refGame.global.resources.getOtherText('age', {param: this.calculateAge(personData.birthdate, personData.deathdate)}) + " " + ";";
        if (this.options.showBirthdate)
            textToSpeech += this.refGame.global.resources.getOtherText('birthdate', {param: personData.birthdate}) + " " + ";";
        if (this.options.showDeathdate)
            textToSpeech += (personData.deathdate !== "" ? this.refGame.global.resources.getOtherText('deathdate', {param: personData.deathdate}) + " " : "") + ";";
        if (this.options.showAddress)
            textToSpeech += this.refGame.global.resources.getOtherText('address', {param: personData.address}) + " " + ";";
        if (this.options.showParents)
            textToSpeech += this.refGame.global.resources.getOtherText('parents', {param: personData.father.firstName + " " + personData.father.lastName + " & " + personData.mother.firstName + " " + personData.mother.lastName}) + " " + ";";
        if (this.options.showChild)
            textToSpeech += (personData.child && personData.child !== "" ? this.refGame.global.resources.getOtherText('child', {param: personData.child}) : "");

        let htmlAlert = "<img src='" + this.refGame.global.resources.getImage(personData.image).getImage().src + "' style='width:20%;'><br>";
        if (this.options.showFirstName)
            htmlAlert += this.refGame.global.resources.getOtherText('firstName', {param: personData.firstName}) + "<br>";
        if (this.options.showLastName)
            htmlAlert += this.refGame.global.resources.getOtherText('lastName', {param: personData.lastName}) + "<br>";
        if (this.options.showAge)
            htmlAlert += this.refGame.global.resources.getOtherText('age', {param: this.calculateAge(personData.birthdate, personData.deathdate)}) + "<br>";
        if (this.options.showBirthdate)
            htmlAlert += this.refGame.global.resources.getOtherText('birthdate', {param: personData.birthdate}) + "<br>";
        if (this.options.showDeathdate)
            htmlAlert += (personData.deathdate !== "" ? this.refGame.global.resources.getOtherText('deathdate', {param: personData.deathdate}) + "<br>" : "");
        if (this.options.showAddress)
            htmlAlert += this.refGame.global.resources.getOtherText('address', {param: personData.address}) + "<br>";
        if (this.options.showParents)
            htmlAlert += this.refGame.global.resources.getOtherText('parents', {param: personData.father.firstName + " " + personData.father.lastName + " & " + personData.mother.firstName + " " + personData.mother.lastName}) + "<br>";
        if (this.options.showChild)
            htmlAlert += (personData.child && personData.child !== "" ? this.refGame.global.resources.getOtherText('child', {param: personData.child}) + "<br>" : "");
        htmlAlert += "<i class=\"fas fa-volume-up fa-3x\" id=\"speaker\" onclick=\"audio.textToSpeechFromTxt('" + textToSpeech + "', '" + this.refGame.global.resources.getLanguage() + "');\"></i>";
        this.refGame.global.util.showAlertPersonnalized({
            title: personData.firstName + " " + personData.lastName,
            html: htmlAlert,
            text: ''
        });
    }

    calculateAge(birthDateString, deathDateString) {
        birthDateString = birthDateString.split('.').reverse().join('-');
        let birthdate = new Date(birthDateString);
        let age;
        let month;
        if (deathDateString !== "") {
            deathDateString = deathDateString.split('.').reverse().join('-');
            let deathdate = new Date(deathDateString);
            age = deathdate.getFullYear() - birthdate.getFullYear();
            month = deathdate.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && deathdate.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return age;
        } else {
            let today = new Date();
            age = today.getFullYear() - birthdate.getFullYear();
            month = today.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return age;
        }
    }

    checkOnMove(character) {
        if (this.canMove)
            if (this.tree.checkIsInCell(character)) {
                if (this.haveCellsSpriteInside()) {
                    this.canMove = false;
                    let count = this.checkWin();
                    if (this.evaluate) {
                        let mark = 0;
                        let barem = JSON.parse(this.refGame.global.resources.getEvaluation()).notes;
                        let perc = (count / this.tree.cells.length) * 100;
                        for (let b of barem) {
                            if (b.min <= perc && b.max >= perc) {
                                mark = b.note;
                            }
                        }
                        this.refGame.global.statistics.updateStats(mark);
                    }
                    let errorCount = this.tree.cells.length - count;
                    if (errorCount !== 0) {
                        this.refGame.global.util.showAlert(
                            'error',
                            this.refGame.global.resources.getOtherText('defeat', {
                                numberErrors: errorCount
                            }),
                            this.refGame.global.resources.getOtherText('error'),
                            undefined,
                            this.exercice ? this.displayInterfaceCallback.bind(this) : this.showTitle.bind(this));
                    } else {
                        this.refGame.global.util.showAlert(
                            'success',
                            this.refGame.global.resources.getOtherText('victory'),
                            this.refGame.global.resources.getOtherText('success'),
                            undefined,
                            this.exercice ? this.displayInterfaceCallback.bind(this) : this.showTitle.bind(this));
                    }
                }
            }

    }

    shuffleArray(array) {
        let currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    checkWin() {
        let charactersInfo = [];
        if (this.refGame.global.resources.getDegre() > 3) {
            charactersInfo.push(this.scenario.enfant.mother.mother.mother);
            charactersInfo.push(this.scenario.enfant.mother.mother.father);
            charactersInfo.push(this.scenario.enfant.mother.father.mother);
            charactersInfo.push(this.scenario.enfant.mother.father.father);
            charactersInfo.push(this.scenario.enfant.father.mother.mother);
            charactersInfo.push(this.scenario.enfant.father.mother.father);
            charactersInfo.push(this.scenario.enfant.father.father.mother);
            charactersInfo.push(this.scenario.enfant.father.father.father);
        }
        charactersInfo.push(this.scenario.enfant.mother.mother);
        charactersInfo.push(this.scenario.enfant.mother.father);
        charactersInfo.push(this.scenario.enfant.father.mother);
        charactersInfo.push(this.scenario.enfant.father.father);
        charactersInfo.push(this.scenario.enfant.mother);
        charactersInfo.push(this.scenario.enfant.father);
        charactersInfo.push(this.scenario.enfant);

        let correctCount = 0;
        for (let i = 0; i < this.tree.cells.length; i++) {
            if (this.tree.checkIsInSpecificCell(this.tree.characters[i], this.tree.cells[i])) {
                if (this.tree.characters[i].info === charactersInfo[i])
                    correctCount++;
            }
        }
        return correctCount;

    }

    sortCharacters(correctOrder) {
        let newOrder = [];
        for (let i = 0; i < correctOrder.length; i++) {
            let searchedCharacter = correctOrder[i];
            for (let character of this.tree.characters) {
                if (character.info === searchedCharacter) {
                    newOrder.push(character);
                    break;
                }
            }
        }
        this.tree.characters = newOrder;
    }

    haveCellsSpriteInside() {
        let count = 0;
        for (let cell of this.tree.cells) {
            for (let character of this.tree.characters) {
                if (this.tree.checkIsInSpecificCell(character, cell)) {
                    count++;
                    break;
                }
            }
            for (let character of this.tree.intruders) {
                if (this.tree.checkIsInSpecificCell(character, cell)) {
                    count++;
                    break;
                }
            }
        }
        return count === this.tree.cells.length;
    }

    displayInterfaceCallback() {
        this.refGame[this.interfaceCallback].show(true);
    }

    setupSignalement() {
        this.refGame.inputs.signal.onclick = function () {
            Swal.fire({
                title: this.refGame.global.resources.getOtherText('signalementTitle'),
                cancelButtonText: this.refGame.global.resources.getOtherText('signalementCancel'),
                showCancelButton: true,
                html:
                    '<label>' + this.refGame.global.resources.getOtherText('signalementReason') + '</label><textarea id="swal-reason" class="swal2-textarea"></textarea>' +
                    '<label>' + this.refGame.global.resources.getOtherText('signalementPassword') + '</label><input type="password" id="swal-password" class="swal2-input">',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-reason').val(),
                            $('#swal-password').val()
                        ])
                    })
                },
                onOpen: function () {
                    $('#swal-reason').focus()
                }
            }).then(function (result) {
                if (result && result.value) {
                    this.refGame.global.resources.signaler(result.value[0], result.value[1], this.currentExerciceId, function (res) {
                        Swal.fire(res.message, '', res.type);
                        if (res.type === 'success') {
                            this.init(this.evaluation);
                            this.show();
                        }
                    }.bind(this));
                }
            }.bind(this));
        }.bind(this);
    }
}
