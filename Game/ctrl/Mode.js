/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }

}