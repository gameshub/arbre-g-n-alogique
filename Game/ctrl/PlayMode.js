/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class PlayMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('play');
        this.refGame = refGame;
        this.setInterfaces({play: new Play(refGame),})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init() {
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show() {
        this.interfaces.play.show();
    }

}