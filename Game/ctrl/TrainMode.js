/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class TrainMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.interfaceCallback = undefined;
        this.setInterfaces({train: new Train(refGame),})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init(evaluate, interfaceCallback=undefined) {
        this.evaluate = evaluate;
        this.interfaceCallback = interfaceCallback;
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show(exercice = undefined) {
        this.interfaces.train.show(this.evaluate,exercice,this.interfaceCallback);
    }

}