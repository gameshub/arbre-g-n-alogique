class Character extends Asset {


    constructor(x, y, width, height, sprite, info, onMove, onDbClick) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.info = info;
        this.onMove = onMove;
        this.onDbClick = onDbClick;
        this.sprite = sprite;
        this.container = new PIXI.Container();
        this.hasFirstClick = false;
        this.data = null;
        this.movable = null;
        this.canMove = true;
        this.init();
    }

    init() {
        this.container.y = 0;
        this.container.x = 0;
        this.container.height = 600;
        this.container.width = 600;

        this.container.removeChildren();
        let sprite = PIXI.Sprite.fromImage(this.sprite.image.src);
        sprite.x = this.x;
        sprite.y = this.y;
        sprite.anchor.x = 0;
        sprite.anchor.y = 0;
        sprite.width = this.width;
        sprite.height = this.height;
        sprite.buttonMode = true;
        sprite.interactive = true;
        sprite.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));
        sprite.on('pointerdown', function () {
            if (this.hasFirstClick) {
                if (this.onDbClick) {
                    this.onDbClick(this);
                }
            } else {
                this.hasFirstClick = true;
                setTimeout(function () {
                    this.hasFirstClick = false;
                }.bind(this), 500);
            }
        }.bind(this));
        this.movable = sprite;
        this.container.addChild(sprite);
    }

    setSprite(sprite){
        this.sprite = sprite;
        this.init();
    }


    onDragStart(event) {
        this.data = event.data;
    }


    onDragEnd() {
        this.data = null;
    }

    onDragMove() {
        if (this.canMove) {
            if (this.data) {
                let newPosition = this.data.getLocalPosition(this.movable.parent);
                newPosition.x = newPosition.x < 0 ? 0 : newPosition.x;
                newPosition.y = newPosition.y < 0 ? 0 : newPosition.y;
                newPosition.x = newPosition.x > 600 ? 600 : newPosition.x;
                newPosition.y = newPosition.y > 600 ? 600 : newPosition.y;
                this.movable.x = newPosition.x - this.width / 2;
                this.movable.y = newPosition.y - this.height / 2;
                this.y = newPosition.y - this.height / 2;
                this.x = newPosition.x - this.width / 2;
                if (this.onMove) {
                    this.onMove(this);
                }
            }
        }
    }

    setCanMove(canMove) {
        this.canMove = canMove;
    }

    getCanMove() {
        return this.canMove;
    }

    setInfo(info){
        this.info = info;
    }

    getInfo(){
        return this.info;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.movable.y = y;
    }

    setX(x) {
        this.x = x;
        this.movable.x = x;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}