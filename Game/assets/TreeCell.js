class TreeCell extends Asset {


    constructor(x, y, width, height, bgIMG, onClick = undefined, cellInfo = undefined) {
        super();
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.bgIMG = bgIMG;
        this.sprite = null;
        this.elements = [];
        this.hasFirstClick = false;
        this.hasSpriteInside = false;
        this.onClick = onClick;
        this.cellInfo = cellInfo;
        this.spriteInside = null;
        this.display();
    }

    setSprite(sprite, info, dbClick) {
        this.sprite = {
            sprite: sprite,
            info: info,
            callback: dbClick
        };
        this.display();
    }

    display() {
        this.elements = [];
        if (this.bgIMG) {
            let cellBg = PIXI.Sprite.fromImage(this.bgIMG.image.src);
            let imgWidth = this.width;
            let imgHeight = (this.bgIMG.getHeight() / this.bgIMG.getWidth()) * this.width;
            this.height = imgHeight;
            let xPos = this.x;
            let yPos = this.y;
            cellBg.anchor.y = 0;
            cellBg.anchor.x = 0.5;
            cellBg.x = xPos;
            cellBg.y = yPos;
            cellBg.height = imgHeight;
            cellBg.width = imgWidth;
            cellBg.interactive = true;
            cellBg.on('pointerdown', function () {
                if (this.onClick)
                    this.onClick(this);
            }.bind(this));
            this.elements.push(cellBg);
        }
        if (this.sprite) {

            let sprite = PIXI.Sprite.fromImage(this.sprite.sprite.image.src);
            let imgHeight = this.height * 0.85;
            let imgWidth = imgHeight / (this.sprite.sprite.getHeight() / this.sprite.sprite.getWidth());
            let xPos = this.x - imgWidth / 2;
            let yPos = this.y + imgHeight * 0.15 / 2;
            sprite.anchor.y = 0;
            sprite.anchor.x = 0;
            sprite.x = xPos;
            sprite.y = yPos;
            sprite.height = imgHeight;
            sprite.width = imgWidth;
            sprite.interactive = true;
            sprite.on('pointerdown', function () {
                if (this.hasFirstClick) {
                    if (this.sprite.callback) {
                        this.sprite.callback(this);
                    } else {
                    }
                } else {
                    this.hasFirstClick = true;
                    setTimeout(function () {
                        this.hasFirstClick = false;
                    }.bind(this), 500);
                }
            }.bind(this));
            this.elements.push(sprite);
        }
    }

    getSpriteInside() {
        return this.spriteInside;
    }

    setSpriteInside(sprite) {
        this.spriteInside = sprite;
    }

    getPixiChildren() {
        return this.elements;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.display();
    }

    setX(x) {
        this.x = x;
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        for (let el of this.elements)
            el.visible = visible;
    }
}