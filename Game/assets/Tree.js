class Tree extends Asset {

    constructor(x, y, width, height, bgIMG) {
        super();
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.bgIMG = bgIMG;
        this.cells = [];
        this.characters = [];
        this.intruders = [];
        this.container = new PIXI.Container();
        this.offsetX = 0;
        this.offsetY = 0;
        this.display();
    }

    addCells(x, y, width, height, cell, sprite, info, dbClick) {
        let treeCell = new TreeCell(x, y, width, height, cell);
        if (sprite)
            treeCell.setSprite(sprite, info, dbClick);
        this.cells.push(treeCell);
        this.display();
    }

    addEmptyCell(x, y, width, height, cell, onClick, cellInfo) {
        let treeCell = new TreeCell(x, y, width, height, cell, onClick, cellInfo);
        this.cells.push(treeCell);
        this.display();
    }

    addCharacter(character, isIntruders) {
        if (isIntruders)
            this.intruders.push(character);
        else
            this.characters.push(character);
        this.display();
    }

    checkIsInCell(character) {
        let isInCell = false;
        let characterX = character.getX() + character.getWidth() / 2;
        let characterY = character.getY() + character.getHeight() / 2;
        for (let cell of this.cells) {
            let cellX = cell.getX() - cell.getWidth() / 2;
            if ((cellX < characterX) && (cellX + cell.getWidth() > characterX) && (cell.getY() < characterY) && (cell.getY() + cell.getHeight() > characterY) && (!cell.getSpriteInside() || cell.getSpriteInside() === character)) {
                character.setX(cellX + (cell.getWidth() - character.getWidth()) / 2);
                character.setY(cell.getY() + (cell.getHeight() - character.getHeight()) / 2);
                cell.setSpriteInside(character);
                isInCell = true;
                break;
            } else if(cell.getSpriteInside() === character){
                cell.setSpriteInside(undefined);
            }
        }
        return isInCell;
    }

    checkIsInSpecificCell(character, cell) {

        let characterX = character.getX() + character.getWidth() / 2;
        let characterY = character.getY() + character.getHeight() / 2;
        let cellX = cell.getX() - cell.getWidth() / 2;
        return (cellX < characterX) && (cellX + cell.getWidth() > characterX) && (cell.getY() < characterY) && (cell.getY() + cell.getHeight() > characterY);
    }

    display() {
        this.container.removeChildren();
        this.container.x = this.x;
        this.container.y = this.y;
        this.container.height = this.height;
        this.container.width = this.width;

        let bg = PIXI.Sprite.fromImage(this.bgIMG.image.src);
        bg.anchor.y = 0;
        bg.anchor.x = 0;
        bg.x = this.x;
        bg.y = this.y;
        bg.height = this.height;
        bg.width = this.width;
        this.container.addChild(bg);

        for (let cell of this.cells)
            for (let el of cell.getPixiChildren())
                this.container.addChild(el);


        for (let character of this.characters)
            for (let el of character.getPixiChildren())
                this.container.addChild(el);

        for (let character of this.intruders) {
            console.log(character);
            for (let el of character.getPixiChildren())
                this.container.addChild(el);
        }
    }

    reset() {
        this.cells = [];
        this.characters = [];
        this.intruders = [];
        this.display();
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.display();
    }

    setX(x) {
        this.x = x;
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}