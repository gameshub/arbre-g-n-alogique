/**
 * @classdesc Bean node. Noeud qui compose une grille de dessin
 * @author Vincent Audergon
 * @version 1.0
 */
class Node {

    /**
     * Constructeur de Node
     * @param {double} x La position x du noeud
     * @param {double} y La position y du noeud
     * @param {double} width La largeur et hauteur du noeud
     * @param {DrawingGrid} refGrid La référence vers la {@link DrawingGrid} (grille de dessin)
     */
    constructor(x, y, width, refGrid) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.refGrid = refGrid;
        this.init();
    }

    /**
     * Initialise le noeud
     */
    init() {

        this.graphics = new PIXI.Graphics();
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.beginFill(0xFFFFFF);
        this.graphics.drawRect(this.x-10,this.y-10, this.width+20,this.width+20);
        this.graphics.endFill(0xFFFFFF);
        this.graphics.on('pointerdown', this.onNodeClicked.bind(this));

        this.point = new PIXI.Graphics();
        this.point.beginFill(0x000000);
        this.point.drawRect(this.x, this.y, this.width, this.width);
        this.point.endFill(0x000000);
        this.point.interactive = true;
        this.point.buttonMode = true;
        this.point.on('pointerdown', this.onNodeClicked.bind(this));
    }

    /**
     * Retourne les coordonées du centre du noeud
     * @return {Point} le centre du noeud
     */
    center() {
        return new Point(this.x + this.width / 2, this.y + this.width / 2);
    }

    /**
     * Méthode de callback appelée lorsqu'un click est détecté sur le noeud
     * @param {Event} e L'événement JavaScript
     */
    onNodeClicked(e) {
        this.refGrid.onNodeClicked(e, this);
    }

    /**
     * Méthode de callback appelée lorsque le click est enfoncé
     * et que le curseur passe au dessus du noeud
     * @deprecated
     * @param {Event} e L'événement JavaScript
     */
    onNodeEncountered(e) {
        this.refGrid.onNodeEncountered(e, this)
    }


}
