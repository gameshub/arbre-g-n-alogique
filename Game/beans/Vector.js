/**
 * @classdesc Object vecteur
 * @author Vincent Audergon
 * @version 1.0
 */
class Vector {

    /**
     * Constructeur de l'objet vector
     * @param {double} x La composante x du vecteur
     * @param {double} y La composante y du vecteur
     * @param {Point} offset L'origine du vecteur
     * @param {string} from Le nom du pont de départ
     * @param {string} to Le nom du point d'arrivé
     * @param {boolean} [calcDirection=true] Indique si la direction doit être calculée, vrai par défaut (facultatif)
     * @example
     *
     *      let v1 = new Vector(1,1,new Point(0,0),'A','B', true); //Avec calcul de la direction
     *      let v2 = new Vector(1,1,new Point(0,0),'A','B', false); //Sans calcul de la direction
     */
    constructor(x, y, offset, from, to, calcDirection = true) {
        /** @type {Point} l'origine du vecteur */
        this.offset = offset;
        /** @type {string} le nom du point de départ */
        this.from = from;
        /** @type {string} le nom du point d'arrivée */
        this.to = to;
        /** @type {Point} le sens du vecteur */
        this.way = new Point(x < 0 ? -1 : 1, y < 0 ? -1 : 1);
        /** @type {double} la composante x du vecteur */
        this.x = Math.abs(x);
        /** @type {double} la composante y du vecteur */
        this.y = Math.abs(y);
        /** @type {double} la norme du vecteur */
        this.norme = VectorUtils.calcNorme(this);
        /** @type {Vector} la direction du vecteur (0 à 180°) */
        this.direction = calcDirection ? VectorUtils.calcDirection(this) : NaN;
        /** @type {Vector} la direction complète du vecteur (0 à 360°) */
        this.fullDirection = calcDirection ? VectorUtils.calcFullDirection(this) : NaN;
    }

    /**
     * Retourne la composante x **signée**
     * @return {double} La composante x
     */
    getTrueX() {
        return this.x * this.way.x;
    }

    /**
     * Retourne la composante y **signée**
     * @return {double} La composante y
     */
    getTrueY() {
        return this.y * this.way.y;
    }

    /**
     * Effectue une addition sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    add(v2) {
        this.x += v2.x;
        this.y += v2.y;
    }

    /**
     * Effectue une soustraction sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    sub(v2) {
        this.x -= v2.x;
        this.y -= v2.y;
    }

    /**
     * Effectue une multiplication sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    mult(v2) {
        this.x *= v2.x;
        this.y *= v2.y;
    }

    /**
     * Effectue une division sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    div(v2) {
        this.x /= v2.x;
        this.y /= v2.y;
    }

    /**
     * Effectue un modulo sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    mod(v2) {
        this.x %= v2.x;
        this.y %= v2.y;
    }

    /**
     * Transforme le vecteur en chaîne de caractères
     * @return {string} la chaîne de caractères
     */
    toString() {
        return `[${this.x}, ${this.y}]`;
    }

    /**
     * Retourne un clone du vecteur
     * @return {Vector} le clone
     */
    clone() {
        return new Vector(this.x, this.y, this.offset, this.name);
    }

}
