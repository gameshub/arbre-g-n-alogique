/**
 * @classdesc Bean Question
 * @author Vincent Audergon
 * @version 1.0
 */
class Question {

    /**
     * Constructeur de Question
     * @param {string[]} label La consigne de la question dans les différentes langues
     * @param {JSON} responses Les réponses possibles
     * @param {JSON} traps Les réponses piège
     * @param {int} type Le type de la question (0 => QCM, 1 => dessin)
     * @param {string} qcmtype Le type de QCM (radio | checkbox)
     * @example
     *
     *      let q1 = new Question({fr: "En français", de:"En allemand"},
     *                            {value:"rect", response: true},
     *                            {value:"square", response: false},
     *                            0, "radio")
     */
    constructor(label, responses, traps, type, qcmtype) {
        this.label = label;
        this.responses = responses;
        this.traps = traps;
        this.type = type;
        this.qcmtype = qcmtype;
    }

}
