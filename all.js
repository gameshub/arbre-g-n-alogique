
/**
 * @classdesc Objet angle
 * @author Vincent Audergon
 * @version 1.0
 */
class Angle {

    /**
     * Constructeur de l'objet Angle
     * @param {double} alpha L'angle alpha (intérieur au polygone)
     * @param {double} beta L'angle beta (exterieur au polygone)
     * @param {string[]} points Les points concercées par l'angle (ABC par ex.)
     * @example
     *
     *      new Angle(90,270,['A','B','C'])
     */
    constructor(alpha, beta, points){
        this.alpha = alpha;
        this.beta = beta;
        this.points = points;
    }

}
/**
 * @classdesc Objet Line, représente une droite en mathématiques
 * @author Vincent Audergon
 * @version 1.0
 */
class Line {

    /**
     * Constructeur de l'objet Line. Il est possible de créer la droite à partir
     * d'un angle et d'un point ou des attributs a et b d'une fonction affine.
     * @param {double|double} angle L'angle de la fonction en **radians** | a si fromAngle est faux
     * @param {Point|double} p1 Un {@link Point} de la fonction | b si fromAngle est faux
     * @param {boolean} [fromAngle=true] Argument facultatif, si la droite est calculée
     *                            à partir d'un angle ou pas, vrai par défaut
     *
     * @example
     *
     *      let l1 = new Line(1,1, false); //A partir de a et b
     *      let l2 = new Line(45,new Point(1,1)) //A partir d'un angle
     */
    constructor(angle, p1, fromAngle = true) {
        if (fromAngle) {
            if (angle % Math.PI !== Math.PI / 2) {
                this.a = Math.round(Math.tan(angle) * 100) / 100;
                this.b = Math.round((p1.y - this.a * p1.x) * 100) / 100;
            } else {
                this.a = Infinity;
                this.b = p1.x;
            }
        } else {
            this.a = angle;
            this.b = p1;
        }
    }

    /**
     * Calcule la coordonnée y en fonction de la coordonnée x
     * Selon l'équation *y = ax+b*
     * @param {double} x La coordonnée x
     * @return {double} La coordonnée y
     */
    calcY(x) {
        if (this.a != Infinity) {
            return this.a * x + this.b;
        } else {
            return x === this.b ? Infinity : NaN;
        }
    }

    /**
     * Vérifie si un point est sur la droite ou non
     * @param {Point} point Le {@link Point} à vérifier
     * @return {boolean} Si le point est sur la droite
     */
    containsPoint(point) {
        return this.calcY(point.x) === point.y || this.calcY(point.x) === Infinity;
    }
}
/**
 * @classdesc Bean node. Noeud qui compose une grille de dessin
 * @author Vincent Audergon
 * @version 1.0
 */
class Node {

    /**
     * Constructeur de Node
     * @param {double} x La position x du noeud
     * @param {double} y La position y du noeud
     * @param {double} width La largeur et hauteur du noeud
     * @param {DrawingGrid} refGrid La référence vers la {@link DrawingGrid} (grille de dessin)
     */
    constructor(x, y, width, refGrid) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.refGrid = refGrid;
        this.init();
    }

    /**
     * Initialise le noeud
     */
    init() {

        this.graphics = new PIXI.Graphics();
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.beginFill(0xFFFFFF);
        this.graphics.drawRect(this.x-10,this.y-10, this.width+20,this.width+20);
        this.graphics.endFill(0xFFFFFF);
        this.graphics.on('pointerdown', this.onNodeClicked.bind(this));

        this.point = new PIXI.Graphics();
        this.point.beginFill(0x000000);
        this.point.drawRect(this.x, this.y, this.width, this.width);
        this.point.endFill(0x000000);
        this.point.interactive = true;
        this.point.buttonMode = true;
        this.point.on('pointerdown', this.onNodeClicked.bind(this));
    }

    /**
     * Retourne les coordonées du centre du noeud
     * @return {Point} le centre du noeud
     */
    center() {
        return new Point(this.x + this.width / 2, this.y + this.width / 2);
    }

    /**
     * Méthode de callback appelée lorsqu'un click est détecté sur le noeud
     * @param {Event} e L'événement JavaScript
     */
    onNodeClicked(e) {
        this.refGrid.onNodeClicked(e, this);
    }

    /**
     * Méthode de callback appelée lorsque le click est enfoncé
     * et que le curseur passe au dessus du noeud
     * @deprecated
     * @param {Event} e L'événement JavaScript
     */
    onNodeEncountered(e) {
        this.refGrid.onNodeEncountered(e, this)
    }


}
/**
 * @classdesc Bean point
 * @author Vincent Audergon
 * @version 1.0
 */
class Point {

    /**
     * Constructeur de l'objet point
     * @param {double} x la composante x du point
     * @param {double} y la composante y du point
     * @param {boolean} [nonconvex=false] indique si l'angle lié au point est convexe ou non (facultatif, faux par défaut)
     */
    constructor(x, y, nonconvex = false) {
        this.x = x;
        this.y = y;
        this.nonconvex = nonconvex;
    }

    /**
     * Additionne les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    add(point) {
        return new Point(this.x + point.x, this.y + point.y);
    }

    /**
     * Soustrait les composantes x et y d'un autre point au point pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    sub(point) {
        return new Point(this.x - point.x, this.y - point.y);
    }

    /**
     * Multiplie les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    mult(point) {
        return new Point(this.x * point.x, this.y * point.y);
    }

    /**
     * Divise les composantes x et y du point par celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    div(point) {
        return new Point(this.x / point.x, this.y / point.y);
    }

    /**
     * Rend un nouveau point avec les valeurs absolues du point (par ex. 1;-1 => 1;1)
     * @return {Point} le nouveau {@link Point}
     */
    abs(){
        return new Point(Math.abs(this.x), Math.abs(this.y));
    }

    /**
     * Indique si deux points sont égaux ou non (composantes égales entre elles)
     * @param {Point} point le deuxième {@link Point}
     * @return {boolean} si les points sont égaux
     */
    equals(point) {
        return this.x === point.x && this.y === point.y;
    }
}
/**
 * @classdesc Bean Question
 * @author Vincent Audergon
 * @version 1.0
 */
class Question {

    /**
     * Constructeur de Question
     * @param {string[]} label La consigne de la question dans les différentes langues
     * @param {JSON} responses Les réponses possibles
     * @param {JSON} traps Les réponses piège
     * @param {int} type Le type de la question (0 => QCM, 1 => dessin)
     * @param {string} qcmtype Le type de QCM (radio | checkbox)
     * @example
     *
     *      let q1 = new Question({fr: "En français", de:"En allemand"},
     *                            {value:"rect", response: true},
     *                            {value:"square", response: false},
     *                            0, "radio")
     */
    constructor(label, responses, traps, type, qcmtype) {
        this.label = label;
        this.responses = responses;
        this.traps = traps;
        this.type = type;
        this.qcmtype = qcmtype;
    }

}
/**
 * @classdesc Bean shape, représente un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class Shape {

    /**
     * Constructeur de l'objet Shape
     * @param {double} x La coordonnée x du polygone sur le canvas
     * @param {double} y La coordonnée y du polygone sur le canvas
     * @param {Point[]} pts La liste des {@link Point} qui composents le polygone
     * @param {double} scale La taille de la forme sur le canvas
     * @param {string} id L'id du polygone
     * @param {string[]} names Le nom de la formes dans toutes les langues
     * @param {Point} [origin={x:0,y:0}] Le {@link Point} d'origine du polygone (facultatif, 0;0 par défaut)
     * @param {number} harmos Degré harmos
     */
    constructor(x, y, pts, scale, id, names, origin, harmos) {
        /** @type {PIXI.Graphics} les graphismes du polygone */
        this.graphics = new PIXI.Graphics();
        this.graphics.x = x;
        this.graphics.y = y;
        /** @type {Point[]} les points qui servent à calculer les propriétés du polygone */
        this.pts = pts;
        /** @type {double} l'ordre de grandeur du polygone (au niveau graphique) */
        this.scale = scale;
        /** @type {string[]} l'id de la forme */
        this.id = id;
        /** @type {string[]} les noms de la forme dans les différentes langues */
        this.names = names;
        /** @type {Point} l'origine du polygone */
        this.origin = origin || new Point(0, 0);
        /** @type {boolean} si les propriétés du polygone ont été calculées */
        this.initialized = false;

        //Propriétés du polygone
        /** @type {Point[]} les points du polygone */
        this.points = [];
        /** @type {Vector[]} les vecteurs qui composent le polygone */
        this.vectors = [];
        /** @type {Angle[]} les angles du polygone */
        this.angles = [];
        /** @type {double[]} les côtés du polygone */
        this.sides = { count: 0 };
        /** @type {int} le nombre de paires de côtés parallèles */
        this.parallels = 0;
        /** @type {Point} le centre de gravité du polygone */
        this.gravityPoint = new Point(0,0);
        /** @type {int} le nombre d'axes de symétrie */
        this.symAxis = 0;
        /** @type {boolean} si le polygone est convexe */
        this.convex = true;
        /** @type {int} le niveau harmos */
        this.harmos = harmos;
        /** @type {Line[]} les médiatrices du polygone */
        this.sideBisections = [];
        /** @type {Line[]} le(s) axe de symétrie du polygone */
        this.axeSym = [];
        /** @type {Line[]} le(s) médianes du polygone */
        this.mediane = [];
        /** @type {Line[]} les bisectrices du polygone */
        this.bisections = [];

        this.nAxeSym = 0;
        this.nPaireCotePara = 0;

        this.gravity = [];
    }

    /**
     * Calcules les différentes propriétés du polygone
     */
    init() {
        //console.log("Start1");

        let lastVect = new Vector;
        let pointSupp = false;
        this.gravityPoint = VectorUtils.calcGravityPoint(this.pts);

        //Calcul du vecteur, de la médiatrice et des points

        //console.log("Pts avant modif:");


        for (let i = 0; i < this.pts.length; i++) {
            let currentPoint = this.pts[i];
            let secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];
            let thirdPoint = null;
            if (i + 2 > this.pts.length){
                thirdPoint = this.pts[1];
            } else if (i + 2 > this.pts.length - 1){
                thirdPoint = this.pts[0];
            } else {
                thirdPoint = this.pts[i + 2];
            }

            let firsVect = new Vector(secondPoint.x - currentPoint.x, secondPoint.y - currentPoint.y,
                new Point(currentPoint.x, currentPoint.y), currentPoint.name, secondPoint.name);
            let secondVect = new Vector(thirdPoint.x - secondPoint.x, thirdPoint.y - secondPoint.y,
                new Point(secondPoint.x, secondPoint.y), secondPoint.name, thirdPoint.name);

            if (firsVect.fullDirection === secondVect.fullDirection) {
                delete this.pts[i + 1];

                for (let j = i + 1; j < this.pts.length - 1; j++) {
                    this.pts[j] = this.pts[j + 1]
                }
                this.pts.pop();
                i--;
            }
        }

        let alphabet = {0:"A",1:"B",2:"C",3:"D",4:"E",5:"F",6:"G",7:"H",8:"I",9:"J",10:"K"};
        for (let i = 0; i < this.pts.length; i++) {
            this.pts[i].name = alphabet[i];
        }

        for (let i = 0; i < this.pts.length; i++) {
            let currentPoint = this.pts[i];
            let secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];

            let vect = new Vector(secondPoint.x - currentPoint.x, secondPoint.y - currentPoint.y,
                new Point(currentPoint.x, currentPoint.y), currentPoint.name, secondPoint.name);

            this.sides.count++;
            this.sides[currentPoint.name + secondPoint.name] = vect.norme;
            let sideBisection = VectorUtils.calcSideBisection(vect);
            if (sideBisection.containsPoint(this.gravityPoint)) this.symAxis++;
            this.sideBisections[`m${currentPoint.name.toLowerCase()}`] = sideBisection;
            this.vectors[currentPoint.name.toLowerCase()] = vect;
            this.points[currentPoint.name] = new Point(currentPoint.x, currentPoint.y, currentPoint.nonconvex);
            if (currentPoint.nonconvex) this.convex = false;
        }


        for (let i = 0; i < this.pts.length; i++) {
            let lastPoint = new Point;
            lastPoint = this.pts[(i - 1 >= 0) ? i - 1 : this.pts.length - 1];

            let lastVector = this.vectors[lastPoint.name.toLowerCase()];
            let currentPoint = this.pts[i];
            let currentVector = this.vectors[currentPoint.name.toLowerCase()];

            let secondPoint = new Point;
            secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];
            //Calcul de l'angle;
            let alpha = VectorUtils.calcAngle(this.vectors[currentPoint.name.toLowerCase()], this.vectors[lastPoint.name.toLowerCase()],false);
            if (currentPoint.nonconvex) alpha = 360 - alpha;
            let angle = new Angle(alpha, 360 - alpha, [lastPoint, currentPoint, secondPoint]);
            this.angles[lastPoint.name + currentPoint.name + secondPoint.name] = angle;
            //Calcul des bissectrices
            let bisection = VectorUtils.calcBisection(angle, lastVector, currentVector);
            if (bisection.containsPoint(this.gravityPoint)) this.symAxis++;
            this.bisections[`b${currentPoint.name.toLowerCase()}`] = bisection;
            //Calcul des parallèles
            if (this.vectors[lastPoint.name.toLowerCase()].direction === this.vectors[secondPoint.name.toLowerCase()].direction) {
                this.parallels++;
            }
        }
        this.parallels /= 2;
        this.initialized = true;
        this.graphics = this.initGraphics();
    }

    /**
     * Retourne un Sprite du polygone. Si une largeur et une hauteur maximale sont données,
     * le sprite sera dimensionné pour rentrer dans ces limites (le plus proche possible)
     * @param {double} maxwidth La largeur maximale du sprite généré (facultatif)
     * @param {double} maxheight La hauteur maximale du sprite généré (facultatif)
     * @param {boolean} lines Défini si les différents axes du polygones doivent apparaître (facultatif)
     * @return {PIXI.Sprite} le sprite du polygone
     */
    initGraphics(maxwidth, maxheight, lines, nAxeSym = 0, nPaireCotePara = 0, axeSym = [], bissec = [], mediatrice = [], mediane = [], gravity = [], affAxeSym = 0, affBissec = 0, affMediatrice = 0, affMediane = 0, affPointNom = false, affgravity = false) {
        let graphics = this.generateGraphics();
        this.axeSym = axeSym;
        this.nAxeSym = nAxeSym;
        this.nPaireCotePara = nPaireCotePara;
        this.bisections = bissec;
        this.sideBisections = mediatrice;
        this.mediane = mediane;
        this.gravity = gravity;

        if (maxwidth && maxheight) {

            let oldscale = this.scale;
            let wscale = this.scale * (maxwidth / graphics.width);
            let hscale = this.scale * (maxheight / graphics.height);
            this.scale = wscale > hscale ? hscale : wscale;
            graphics = this.generateGraphics(lines, affAxeSym, affBissec, affMediatrice, affMediane, affPointNom, affgravity);
            this.scale = oldscale;
        }
        graphics.anchor.set(0.5);
        graphics.interactive = true;
        graphics.buttonMode = true;
        return graphics;
    }

    /**
     * Génère un sprite à partir des points du polygone.
     * Il est possible de donner une liste de droites à afficher sur le polygone.
     * @param {Line[]} lines La liste des {@link Line} à afficher (facultatif)
     * @return {PIXI.Sprite} le sprite généré
     */
    generateGraphics(lines = false, affAxeSym = 0, affBissec = 0, affMediatrice = 0, affMediane = 0, affPointNom = false, affgravity = false) {
        let graphics = new PIXI.Graphics();
        graphics.beginFill(this.getRandomColor());
        graphics.lineStyle(1, 0x00, 1);
        graphics.moveTo(this.origin.x, this.origin.y);
        let nomPoint = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        let index = 0;

        for (let point in this.points) {
            graphics.lineTo(this.points[point].x * this.scale, this.points[point].y * this.scale);

            if (affPointNom){
                let pointNom = new PIXI.Text(nomPoint[index], {fontFamily: 'Arial', fontSize: 15, fill: 0x00, align: 'left'});
                index++;
                pointNom.position.x = this.points[point].x * this.scale;
                pointNom.position.y = this.points[point].y * this.scale - 10;
                graphics.addChild(pointNom);
            }
        }
        graphics.lineTo(this.origin.x, this.origin.y);
        graphics.endFill();
        let polygon = new PIXI.Sprite(graphics.generateCanvasTexture());
        if (lines) {

            if (affMediatrice !== 0){
                // Affiche la ou les médiatrices
                if (affMediatrice === -1){
                    // Affiche toutes les médiatrices
                    for (let i = 0; i < this.sideBisections.length; i++) {
                        let sBis = this.sideBisections[i];
                        let x1 = sBis["x1"];
                        let y1 = sBis["y1"];
                        let x2 = sBis["x2"];
                        let y2 = sBis["y2"];

                        graphics.lineStyle(3,0x0099ff,1);
                        graphics.beginFill(0x0099ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiatrice
                    let sBis = this.sideBisections[affMediatrice - 1];
                    let sbisNom = new PIXI.Text(sBis["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = sBis["x1"];
                    let y1 = sBis["y1"];
                    let x2 = sBis["x2"];
                    let y2 = sBis["y2"];

                    graphics.lineStyle(3,0x0099ff,1);
                    graphics.beginFill(0x0099ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    sbisNom.x = (x1 + 0.02) * this.scale;
                    sbisNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(sbisNom);

                    graphics.endFill();
                }
            }

            if (affBissec !== 0){
                // Affiche toutes les bissectrices
                if (affBissec === -1){
                    for (let i = 0; i < this.bisections.length; i++) {
                        let bis = this.bisections[i];

                        let x1 = bis["x1"];
                        let y1 = bis["y1"];
                        let x2 = bis["x2"];
                        let y2 = bis["y2"];

                        graphics.lineStyle(3,0x33cc33,1);
                        graphics.beginFill(0x33cc33);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une bissectrice
                    let bis = this.bisections[affBissec - 1];
                    let bisNom = new PIXI.Text(bis["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = bis["x1"];
                    let y1 = bis["y1"];
                    let x2 = bis["x2"];
                    let y2 = bis["y2"];

                    graphics.lineStyle(3,0x33cc33,1);
                    graphics.beginFill(0x33cc33);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    bisNom.x = (x1 + 0.02) * this.scale;
                    bisNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(bisNom);

                    graphics.endFill();
                }
            }

            if (affMediane !== 0){
                if (affMediane === -1){
                    // Affiche toutes les médianes
                    for (let i = 0; i < this.mediane.length; i++) {
                        let med = this.mediane[i];

                        let x1 = med["x1"];
                        let y1 = med["y1"];
                        let x2 = med["x2"];
                        let y2 = med["y2"];

                        graphics.lineStyle(3,0xcc00ff,1);
                        graphics.beginFill(0xcc00ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiane
                    let med = this.mediane[affMediane - 1];
                    let medNom = new PIXI.Text(med["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = med["x1"];
                    let y1 = med["y1"];
                    let x2 = med["x2"];
                    let y2 = med["y2"];

                    graphics.lineStyle(3,0xcc00ff,1);
                    graphics.beginFill(0xcc00ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    medNom.x = (x1 + 0.02) * this.scale;
                    medNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(medNom);

                    graphics.endFill();
                }
            }

            if (affAxeSym !== 0){
                if (affAxeSym === -1){
                    // Affiche les axes de symétrie
                    for (let i = 0; i < this.axeSym.length; i++) {
                        let sym = this.axeSym[i];

                        let x1 = sym["x1"];
                        let y1 = sym["y1"];
                        let x2 = sym["x2"];
                        let y2 = sym["y2"];

                        graphics.lineStyle(2,0xbf4080,1);
                        graphics.beginFill(0xbf4080);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    }
                } else {
                    // Affiche un axe de symétrie
                    let sym = this.axeSym[affAxeSym - 1];
                    let symNom = new PIXI.Text(sym["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = sym["x1"];
                    let y1 = sym["y1"];
                    let x2 = sym["x2"];
                    let y2 = sym["y2"];

                    graphics.lineStyle(2,0xbf4080,1);
                    graphics.beginFill(0xbf4080);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    symNom.x = (x1 + 0.02) * this.scale;
                    symNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(symNom);
                }
            }

            if (affgravity){
                let grav = this.gravity[0];
                if (typeof grav !== 'undefined'){
                    let centre = new PIXI.Graphics();
                    console.log(grav);
                    centre.beginFill(0xfaae0a);
                    centre.lineStyle(5, 0xfaae0a);
                    centre.drawRect(grav.x * this.scale,grav.y * this.scale,5,5);
                    graphics.addChild(centre);
                }
            }

            // console.log('LINES', lines, this.sideBisections);
        }
        return new PIXI.Sprite(graphics.generateCanvasTexture());
    }

    /**
     * Retourne le nom du polygone dans la langue demandée
     * @param {string} lang La langue désirée
     * @return {string} le nom du polygone
     */
    getName(lang) {
        // console.log(this.names);
        // console.log(lang);
        return this.names[lang];
    }

    /**
     * Retourne une couleur aléatoire parmis un set de couleurs prédéfinies
     * @return {int} la couleur en hexadécimal
     */
    getRandomColor() {
        let colors = [0xff9999, 0xff80ff, 0x99bbff, 0xadebad, 0xffdd99, 0xecb3ff, 0xb3ccff, 0xb3ffff, 0xb3b3ff, 0xc2efc2];
        let random = Math.floor(Math.random() * colors.length) + 1;
        return colors[random - 1];
    }

}
/**
 * @classdesc Object vecteur
 * @author Vincent Audergon
 * @version 1.0
 */
class Vector {

    /**
     * Constructeur de l'objet vector
     * @param {double} x La composante x du vecteur
     * @param {double} y La composante y du vecteur
     * @param {Point} offset L'origine du vecteur
     * @param {string} from Le nom du pont de départ
     * @param {string} to Le nom du point d'arrivé
     * @param {boolean} [calcDirection=true] Indique si la direction doit être calculée, vrai par défaut (facultatif)
     * @example
     *
     *      let v1 = new Vector(1,1,new Point(0,0),'A','B', true); //Avec calcul de la direction
     *      let v2 = new Vector(1,1,new Point(0,0),'A','B', false); //Sans calcul de la direction
     */
    constructor(x, y, offset, from, to, calcDirection = true) {
        /** @type {Point} l'origine du vecteur */
        this.offset = offset;
        /** @type {string} le nom du point de départ */
        this.from = from;
        /** @type {string} le nom du point d'arrivée */
        this.to = to;
        /** @type {Point} le sens du vecteur */
        this.way = new Point(x < 0 ? -1 : 1, y < 0 ? -1 : 1);
        /** @type {double} la composante x du vecteur */
        this.x = Math.abs(x);
        /** @type {double} la composante y du vecteur */
        this.y = Math.abs(y);
        /** @type {double} la norme du vecteur */
        this.norme = VectorUtils.calcNorme(this);
        /** @type {Vector} la direction du vecteur (0 à 180°) */
        this.direction = calcDirection ? VectorUtils.calcDirection(this) : NaN;
        /** @type {Vector} la direction complète du vecteur (0 à 360°) */
        this.fullDirection = calcDirection ? VectorUtils.calcFullDirection(this) : NaN;
    }

    /**
     * Retourne la composante x **signée**
     * @return {double} La composante x
     */
    getTrueX() {
        return this.x * this.way.x;
    }

    /**
     * Retourne la composante y **signée**
     * @return {double} La composante y
     */
    getTrueY() {
        return this.y * this.way.y;
    }

    /**
     * Effectue une addition sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    add(v2) {
        this.x += v2.x;
        this.y += v2.y;
    }

    /**
     * Effectue une soustraction sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    sub(v2) {
        this.x -= v2.x;
        this.y -= v2.y;
    }

    /**
     * Effectue une multiplication sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    mult(v2) {
        this.x *= v2.x;
        this.y *= v2.y;
    }

    /**
     * Effectue une division sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    div(v2) {
        this.x /= v2.x;
        this.y /= v2.y;
    }

    /**
     * Effectue un modulo sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    mod(v2) {
        this.x %= v2.x;
        this.y %= v2.y;
    }

    /**
     * Transforme le vecteur en chaîne de caractères
     * @return {string} la chaîne de caractères
     */
    toString() {
        return `[${this.x}, ${this.y}]`;
    }

    /**
     * Retourne un clone du vecteur
     * @return {Vector} le clone
     */
    clone() {
        return new Vector(this.x, this.y, this.offset, this.name);
    }

}
/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class Button extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: fgColor, align: 'center'})

        this.height = 0;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update(){
        let buttonWidth = this.getWidth();
        let buttonHeight = this.lbl.height * 1.5;
        this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
class Character extends Asset {


    constructor(x, y, width, height, sprite, info, onMove, onDbClick) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.info = info;
        this.onMove = onMove;
        this.onDbClick = onDbClick;
        this.sprite = sprite;
        this.container = new PIXI.Container();
        this.hasFirstClick = false;
        this.data = null;
        this.movable = null;
        this.canMove = true;
        this.init();
    }

    init() {
        this.container.y = 0;
        this.container.x = 0;
        this.container.height = 600;
        this.container.width = 600;

        this.container.removeChildren();
        let sprite = PIXI.Sprite.fromImage(this.sprite.image.src);
        sprite.x = this.x;
        sprite.y = this.y;
        sprite.anchor.x = 0;
        sprite.anchor.y = 0;
        sprite.width = this.width;
        sprite.height = this.height;
        sprite.buttonMode = true;
        sprite.interactive = true;
        sprite.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));
        sprite.on('pointerdown', function () {
            if (this.hasFirstClick) {
                if (this.onDbClick) {
                    this.onDbClick(this);
                }
            } else {
                this.hasFirstClick = true;
                setTimeout(function () {
                    this.hasFirstClick = false;
                }.bind(this), 500);
            }
        }.bind(this));
        this.movable = sprite;
        this.container.addChild(sprite);
    }

    setSprite(sprite){
        this.sprite = sprite;
        this.init();
    }


    onDragStart(event) {
        this.data = event.data;
    }


    onDragEnd() {
        this.data = null;
    }

    onDragMove() {
        if (this.canMove) {
            if (this.data) {
                let newPosition = this.data.getLocalPosition(this.movable.parent);
                newPosition.x = newPosition.x < 0 ? 0 : newPosition.x;
                newPosition.y = newPosition.y < 0 ? 0 : newPosition.y;
                newPosition.x = newPosition.x > 600 ? 600 : newPosition.x;
                newPosition.y = newPosition.y > 600 ? 600 : newPosition.y;
                this.movable.x = newPosition.x - this.width / 2;
                this.movable.y = newPosition.y - this.height / 2;
                this.y = newPosition.y - this.height / 2;
                this.x = newPosition.x - this.width / 2;
                if (this.onMove) {
                    this.onMove(this);
                }
            }
        }
    }

    setCanMove(canMove) {
        this.canMove = canMove;
    }

    getCanMove() {
        return this.canMove;
    }

    setInfo(info){
        this.info = info;
    }

    getInfo(){
        return this.info;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.movable.y = y;
    }

    setX(x) {
        this.x = x;
        this.movable.x = x;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;

        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;


        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();

        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);

        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);


        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}
/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));
        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {
                this.nodes.push(new Node(c * this.width, l * this.width, this.width / 4.5, this));
            }
        }
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let p = new Point(node.x / this.width - this.origin.x, node.y / this.width - this.origin.y);
        p.name = String.fromCharCode(65 + this.points.length);
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        this.stage.removeChild(this.line);
        this.initLine(node);
        let straightLine = new PIXI.Graphics();
        this.strLines.push(straightLine);
        this.stage.addChild(straightLine);
        straightLine.lineStyle(6, 0xFF, 1);
        straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
        straightLine.lineTo(node.center().x, node.center().y);
        this.lastnode = node;
        if (this.onLineDrawn) this.onLineDrawn();
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if(n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        this.lastnode = node;
        this.initLine(node);
        this.drawing = true;
        this.points.push();
        this.origin = this.createNextPoint(node);
        let p = new Point(0, 0);
        p.name = this.origin.name;
        this.points.push(p);
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        this.drawing = false;
        this.stage.removeChild(this.line);
        this.reset();
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            // console.log("Diagonale : ");
            // if (len >= 2) {
            //     let val1 = this.points[len - 2].sub(this.points[len - 1]);
            //     let val2 =  this.points[len - 1].sub(point);
            //     console.log("############################################################");
            //     console.log("Point len - 2 : ");
            //     console.log(this.points[len - 2]);
            //     console.log("Point len - 1 : ");
            //     console.log(this.points[len - 1]);
            //     console.log("Point :");
            //     console.log(point);
            //     console.log("len-2 - len-1 :");
            //     console.log(val1);
            //     console.log("len-1 - len :");
            //     console.log(val2);
            //
            //     if ((this.points[len - 1].x === this.points[len - 2].x && this.points[len - 1].x === point.x) || //Al. horizontal
            //         (this.points[len - 1].y === this.points[len - 2].y && this.points[len - 1].y === point.y) || //Al. vertical
            //         (val1.equals(val2))) { //Al. diagonale
            //         //Si trois points sont allignés on ne crée pas un nouveau mais on déplace le dernier
            //         if (point.equals(this.points[0]) && len >= 3) {
            //             //Si c'est le dernier point, on efface le dernier
            //             this.points.pop();
            //             this.pointsCalcul.push(point);
            //         } else if (!this.containsPoint(point)) {
            //             //Si c'est pas le dernier et qu'il n'exite pas encore on déplace le dernier
            //             point.name = this.points[len - 1].name;
            //             this.points[len - 1] = point;
            //             this.pointsCalcul.push(point);
            //         }
            //     } else if (!this.containsPoint(point)) {
            //         this.points.push(point);
            //         this.pointsCalcul.push(point);
            //     }
            // }
            if (!this.containsPoint(point)) {
                this.points.push(point);
            }
            if (this.points.length >= 3 && point.equals(this.points[0])) { //Sinon, si il correspond à l'origine
                //Fin de la forme

                this.drawStrLine(node);
                this.onShapeCreated(new Shape(0, 0, this.points, this.width, 'unknown', {}, this.points[0]));
                this.reset();
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Défini la fonction de callback lorsqu'un polygone est créé sur le canvas
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                console.log(this.touchScrollLimiter);
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        console.log(this.elements);

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}
class Select extends Asset{


    constructor(height, width, x, y) {
        super();

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        this.button = new Button(x,y,'SELECT',0xFFFFFF,0x000000, false,width);
        this.container = new PIXI.Container();

        this.currentIndex = 0;
        this.elements = [];

        this.button.setOnClick(function () {
            this.container.visible = !this.container.visible;
        }.bind(this));

        this.container.visible = true;
    }

    addElement(element){
        element.setSelect(this);
        this.elements.push(element);
        this.build();
    }


    onClick(data){
        console.log(data);
    }

    build(){
        this.container.removeChildren(0);
        this.container.width = this.width;
        this.container.height = this.height;
        this.container.x = this.x-this.width/2;
        this.container.y = this.y+30;


        let currentY = 0;
        for(let element of this.elements){
            element.build(this.width/2, currentY, this.width);
            currentY+=element.getHeight();
            for(let child of element.getPixiChildren()){
                this.container.addChild(child);
            }
        }


    }


    resize(height, width){
        this.height = height;
        this.width = width;
        this.build();
    }

    setPosition(x,y){
        this.x = x;
        this.y = y;
        this.build();
    }

    getPixiChildren() {
        let elements = [];
        for (let e of this.button.getPixiChildren()){
            elements.push(e);
        }
        elements.push(this.container);
        return elements;
    }



    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        this.button.setVisible(visible);
        this.container.visible = visible;
    }
}
class SelectItem extends Asset{

    constructor(text, data=null) {
        super();
        this.selectRef = null;
        this.button = null;
        this.text = text;
        this.data = data;
        this.build(0,0,100);
    }


    build(x,y,width){
        this.button = new Button(x,y,this.text,0xFFFFFF,0x000000, false,width);
        this.button.setOnClick(function () {
            this.selectRef.onClick(this.data);
        }.bind(this));
    }


    getPixiChildren() {
        return this.button ? this.button.getPixiChildren():[];
    }

    setText(text){
        this.text = text;
    }

    setSelect(ref){
        this.selectRef = ref;
    }

    getData(){
        return data;
    }

    setData(data){
        this.data = data;
    }

    getHeight(){
        return this.button != null ? this.button.getHeight() : 0;
    }


    getY() {
        this.button.getY();
    }

    getX() {
        this.button.getX();
    }

    setVisible(visible) {
        this.button.setVisible(visible);
    }
}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class ToggleButton extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {boolean} fitToText Taille du bouton en fonction du texte
     * @param {number} width Taille forcée du bouton
     */
    constructor(x, y, label, fitToText=false, width = 150) {
        super();

        this.fitToText = fitToText;
        this.refToggleGroup = null;
        this.width = width;
        this.height = 0;
        this._data = null;
        this.currentState = 'inactive';
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;

        this.lastOnClick = function(){
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center' })
            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center' })
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center' })
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        for (let state of Object.keys(this.states)){
            this.states[state].lbl.text = text;
        }
        this.update();
    }


    update(){
        for (let state of Object.keys(this.states)){
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width): this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if(state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if(state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.set(0.5);
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
        }
    }

    toggle(){
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.disabled.graphics,
            this.states.disabled.lbl];
    }

    updateView(){
        for (let state of Object.keys(this.states)){
            if(state === this.currentState){
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
            } else{
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
            }
        }
    }

    isActive(){
        return this.currentState === 'active';
    }

    isEnabled(){
        return this.currentState !== 'disabled';
    }

    set data(data){
        this._data = data;
    }

    get data(){
        return this._data;
    }


    show(){
        this.updateView();
    }

    hide(){
        for (let element of this.getPixiChildren()){
            element.visible = false;
        }
    }

    disable(){
        this.currentState = 'disabled';
        this.onClick = function () {};
        this.updateView();
    }

    enable(){
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref){
        this.refToggleGroup = ref;
    }

    updateFont(font){
        for(let state of Object.keys(this.states)){
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()){
            element.visible = visible;
        }
    }


    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}
class Tree extends Asset {

    constructor(x, y, width, height, bgIMG) {
        super();
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.bgIMG = bgIMG;
        this.cells = [];
        this.characters = [];
        this.intruders = [];
        this.container = new PIXI.Container();
        this.offsetX = 0;
        this.offsetY = 0;
        this.display();
    }

    addCells(x, y, width, height, cell, sprite, info, dbClick) {
        let treeCell = new TreeCell(x, y, width, height, cell);
        if (sprite)
            treeCell.setSprite(sprite, info, dbClick);
        this.cells.push(treeCell);
        this.display();
    }

    addEmptyCell(x, y, width, height, cell, onClick, cellInfo) {
        let treeCell = new TreeCell(x, y, width, height, cell, onClick, cellInfo);
        this.cells.push(treeCell);
        this.display();
    }

    addCharacter(character, isIntruders) {
        if (isIntruders)
            this.intruders.push(character);
        else
            this.characters.push(character);
        this.display();
    }

    checkIsInCell(character) {
        let isInCell = false;
        let characterX = character.getX() + character.getWidth() / 2;
        let characterY = character.getY() + character.getHeight() / 2;
        for (let cell of this.cells) {
            let cellX = cell.getX() - cell.getWidth() / 2;
            if ((cellX < characterX) && (cellX + cell.getWidth() > characterX) && (cell.getY() < characterY) && (cell.getY() + cell.getHeight() > characterY) && (!cell.getSpriteInside() || cell.getSpriteInside() === character)) {
                character.setX(cellX + (cell.getWidth() - character.getWidth()) / 2);
                character.setY(cell.getY() + (cell.getHeight() - character.getHeight()) / 2);
                cell.setSpriteInside(character);
                isInCell = true;
                break;
            } else if(cell.getSpriteInside() === character){
                cell.setSpriteInside(undefined);
            }
        }
        return isInCell;
    }

    checkIsInSpecificCell(character, cell) {

        let characterX = character.getX() + character.getWidth() / 2;
        let characterY = character.getY() + character.getHeight() / 2;
        let cellX = cell.getX() - cell.getWidth() / 2;
        return (cellX < characterX) && (cellX + cell.getWidth() > characterX) && (cell.getY() < characterY) && (cell.getY() + cell.getHeight() > characterY);
    }

    display() {
        this.container.removeChildren();
        this.container.x = this.x;
        this.container.y = this.y;
        this.container.height = this.height;
        this.container.width = this.width;

        let bg = PIXI.Sprite.fromImage(this.bgIMG.image.src);
        bg.anchor.y = 0;
        bg.anchor.x = 0;
        bg.x = this.x;
        bg.y = this.y;
        bg.height = this.height;
        bg.width = this.width;
        this.container.addChild(bg);

        for (let cell of this.cells)
            for (let el of cell.getPixiChildren())
                this.container.addChild(el);


        for (let character of this.characters)
            for (let el of character.getPixiChildren())
                this.container.addChild(el);

        for (let character of this.intruders) {
            console.log(character);
            for (let el of character.getPixiChildren())
                this.container.addChild(el);
        }
    }

    reset() {
        this.cells = [];
        this.characters = [];
        this.intruders = [];
        this.display();
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.display();
    }

    setX(x) {
        this.x = x;
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
class TreeCell extends Asset {


    constructor(x, y, width, height, bgIMG, onClick = undefined, cellInfo = undefined) {
        super();
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.bgIMG = bgIMG;
        this.sprite = null;
        this.elements = [];
        this.hasFirstClick = false;
        this.hasSpriteInside = false;
        this.onClick = onClick;
        this.cellInfo = cellInfo;
        this.spriteInside = null;
        this.display();
    }

    setSprite(sprite, info, dbClick) {
        this.sprite = {
            sprite: sprite,
            info: info,
            callback: dbClick
        };
        this.display();
    }

    display() {
        this.elements = [];
        if (this.bgIMG) {
            let cellBg = PIXI.Sprite.fromImage(this.bgIMG.image.src);
            let imgWidth = this.width;
            let imgHeight = (this.bgIMG.getHeight() / this.bgIMG.getWidth()) * this.width;
            this.height = imgHeight;
            let xPos = this.x;
            let yPos = this.y;
            cellBg.anchor.y = 0;
            cellBg.anchor.x = 0.5;
            cellBg.x = xPos;
            cellBg.y = yPos;
            cellBg.height = imgHeight;
            cellBg.width = imgWidth;
            cellBg.interactive = true;
            cellBg.on('pointerdown', function () {
                if (this.onClick)
                    this.onClick(this);
            }.bind(this));
            this.elements.push(cellBg);
        }
        if (this.sprite) {

            let sprite = PIXI.Sprite.fromImage(this.sprite.sprite.image.src);
            let imgHeight = this.height * 0.85;
            let imgWidth = imgHeight / (this.sprite.sprite.getHeight() / this.sprite.sprite.getWidth());
            let xPos = this.x - imgWidth / 2;
            let yPos = this.y + imgHeight * 0.15 / 2;
            sprite.anchor.y = 0;
            sprite.anchor.x = 0;
            sprite.x = xPos;
            sprite.y = yPos;
            sprite.height = imgHeight;
            sprite.width = imgWidth;
            sprite.interactive = true;
            sprite.on('pointerdown', function () {
                if (this.hasFirstClick) {
                    if (this.sprite.callback) {
                        this.sprite.callback(this);
                    } else {
                    }
                } else {
                    this.hasFirstClick = true;
                    setTimeout(function () {
                        this.hasFirstClick = false;
                    }.bind(this), 500);
                }
            }.bind(this));
            this.elements.push(sprite);
        }
    }

    getSpriteInside() {
        return this.spriteInside;
    }

    setSpriteInside(sprite) {
        this.spriteInside = sprite;
    }

    getPixiChildren() {
        return this.elements;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.display();
    }

    setX(x) {
        this.x = x;
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        for (let el of this.elements)
            el.visible = visible;
    }
}

/**
 * @classdesc Définition d'une interface (ihm)
 * @author Vincent Audergon
 * @version 1.0
 */
class Interface {

    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                this.scene.addChild(el);
            }
        }
        this.refGame.showScene(this.scene);
    }

    /**
     * Affiche l'ihm
     */
    show() { }

}
/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */
class Create extends Interface {


    constructor(refGame) {
        super(refGame, "create");

        this.currentTutorialText = 'create';

        this.refGame = refGame;
        this.options = {};
        this.createdCharacter = [];

        this.names = {
            fr: '',
            de: '',
            en: '',
            it: ''
        };

        this.intruders = [];
        this.intrudersList = [];

        this.tree = new Tree(0, 0, 600, 600, this.refGame.global.resources.getImage('tree'));
        this.mainTitle = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
        this.mainTitle.anchor.set(0.5);
        this.mainTitle.x = 300;
        this.mainTitle.y = 150;
        this.startButton.setOnClick(this.showTree.bind(this));

        this.endButton = new Button(525, 585, '', 0x007bff, 0xFFFFFF, true);
        this.endButton.setOnClick(this.endInsertion.bind(this));

        this.nameButton = new Button(525, 585, '', 0x007bff, 0xFFFFFF, true);
        this.nameButton.setOnClick(this.displayName.bind(this));

        this.editNameButton = new Button(300, 100, '', 0x028700, 0xFFFFFF, true);
        this.editNameButton.setOnClick(this.editName.bind(this));

        this.nextBtn = new Button(525, 585, '', 0x028700, 0xFFFFFF, true);
        this.nextBtn.setOnClick(this.showOptions.bind(this));
        this.addIntruderBtn = new Button(300, 585, '', 0x028700, 0xFFFFFF, true);
        this.addIntruderBtn.setOnClick(this.addIntruder.bind(this));

        this.characters = {};

        this.pages = {
            submission: {
                submissionTitle: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }),
                btnSubmit: new Button(300, 200, '', 0x28a745, 0xFFFFFF, false, 250),
                btnDelete: new Button(300, 300, '', 0xdc3545, 0xFFFFFF, false, 250),
                btnTest: new Button(300, 400, '', 0x007bff, 0xFFFFFF, false, 250),
                backToName: new Button(75, 585, '', 0x007bff, 0xFFFFFF, true)
            }, name: {
                fr: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }), de: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }), en: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }), it: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }),
                backToOptions: new Button(75, 585, '', 0x007bff, 0xFFFFFF, true)
            },
            options: {
                showLastName: new CheckBox(150, 150, '', 0.9),
                showFirstName: new CheckBox(150, 200, '', 0.9),
                showAge: new CheckBox(150, 250, '', 0.9),
                showBirthdate: new CheckBox(150, 300, '', 0.9),
                showDeathdate: new CheckBox(150, 350, '', 0.9),
                showAddress: new CheckBox(150, 400, '', 0.9),
                showParents: new CheckBox(150, 450, '', 0.9),
                showChild: new CheckBox(150, 500, '', 0.9),
                optionsTitle: new PIXI.Text('', {
                    fontFamily: 'Arial',
                    fontSize: 24,
                    fill: 0x000000,
                    align: 'center'
                }),
                backToTree: new Button(75, 585, '', 0x007bff, 0xFFFFFF, true)
            }
        };


        this.pages.submission.submissionTitle.anchor.set(0.5);
        this.pages.submission.submissionTitle.x = 300;
        this.pages.submission.submissionTitle.y = 100;
        this.pages.submission.btnDelete.setOnClick(this.handleDeleteExercice.bind(this));
        this.pages.submission.btnTest.setOnClick(this.handleTestExercice.bind(this));
        this.pages.submission.btnSubmit.setOnClick(this.handleSubmitExercice.bind(this));
        this.pages.options.optionsTitle.anchor.set(0.5);
        this.pages.options.optionsTitle.x = 300;
        this.pages.options.optionsTitle.y = 75;

        this.pages.options.backToTree.setOnClick(this.showTree.bind(this));
        this.pages.submission.backToName.setOnClick(this.displayName.bind(this));

        this.pages.name.backToOptions.setOnClick(this.showOptions.bind(this));

        this.pages.name.fr.anchor.set(0.5);
        this.pages.name.fr.x = 300;
        this.pages.name.fr.y = 200;
        this.pages.name.de.anchor.set(0.5);
        this.pages.name.de.x = 300;
        this.pages.name.de.y = 275;
        this.pages.name.en.anchor.set(0.5);
        this.pages.name.en.x = 300;
        this.pages.name.en.y = 350;
        this.pages.name.it.anchor.set(0.5);
        this.pages.name.it.x = 300;
        this.pages.name.it.y = 425;

        this.elements.push(this.tree,
            this.mainTitle,
            this.startButton,
            this.endButton,
            this.pages.submission.btnSubmit,
            this.pages.submission.btnDelete,
            this.pages.submission.btnTest,
            this.pages.submission.submissionTitle,
            this.nextBtn,
            this.pages.options.showLastName,
            this.pages.options.showFirstName,
            this.pages.options.showAge,
            this.pages.options.showBirthdate,
            this.pages.options.showDeathdate,
            this.pages.options.showParents,
            this.pages.options.showAddress,
            this.pages.options.showChild,
            this.pages.options.optionsTitle,
            this.pages.options.backToTree,
            this.pages.submission.backToName,
            this.addIntruderBtn,
            this.nameButton,
            this.editNameButton,
            this.pages.name.backToOptions,
            this.pages.name.fr,
            this.pages.name.en,
            this.pages.name.it,
            this.pages.name.de
        );
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show(isFromTrainMode = false) {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        this.clear();
        if (isFromTrainMode) {
            this.endInsertion();
        } else {
            this.showTitle();
            this.reset();
            this.generateTree(this.refGame.global.resources.getDegre());
        }
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        if (this.currentTutorialText && this.currentTutorialText !== '')
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentTutorialText));
        this.startButton.setText(this.refGame.global.resources.getOtherText('startMode'));
        this.mainTitle.text = this.refGame.global.resources.getOtherText('createModeTitle');
        this.endButton.setText(this.refGame.global.resources.getOtherText('endButton'));
        this.nextBtn.setText(this.refGame.global.resources.getOtherText('nextBtn'));
        this.pages.submission.btnSubmit.setText(this.refGame.global.resources.getOtherText('submit'));
        this.pages.submission.btnDelete.setText(this.refGame.global.resources.getOtherText('delete'));
        this.pages.submission.btnTest.setText(this.refGame.global.resources.getOtherText('test'));
        this.pages.submission.submissionTitle.text = this.refGame.global.resources.getOtherText('submissionTitle');

        this.pages.options.showLastName.setText(this.refGame.global.resources.getOtherText('showLastName'));
        this.pages.options.showFirstName.setText(this.refGame.global.resources.getOtherText('showFirstName'));
        this.pages.options.showAge.setText(this.refGame.global.resources.getOtherText('showAge'));
        this.pages.options.showBirthdate.setText(this.refGame.global.resources.getOtherText('showBirthdate'));
        this.pages.options.showDeathdate.setText(this.refGame.global.resources.getOtherText('showDeathdate'));
        this.pages.options.showParents.setText(this.refGame.global.resources.getOtherText('showParents'));
        this.pages.options.showAddress.setText(this.refGame.global.resources.getOtherText('showAddress'));
        this.pages.options.showChild.setText(this.refGame.global.resources.getOtherText('showChild'));
        this.pages.options.optionsTitle.text = this.refGame.global.resources.getOtherText('optionsTitle');

        this.pages.submission.backToName.setText(this.refGame.global.resources.getOtherText('back'));
        this.pages.options.backToTree.setText(this.refGame.global.resources.getOtherText('back'));
        this.addIntruderBtn.setText(this.refGame.global.resources.getOtherText('addIntruder'));
        this.editNameButton.setText(this.refGame.global.resources.getOtherText('editName'));
        this.nameButton.setText(this.refGame.global.resources.getOtherText('nextBtn'));
        this.pages.name.backToOptions.setText(this.refGame.global.resources.getOtherText('back'));

        this.pages.name.fr.text = this.names.fr && this.names.fr !== '' ? this.names.fr : this.refGame.global.resources.getOtherText('noNameFor', {
            lang: this.refGame.global.resources.getOtherText('french')
        });
        this.pages.name.de.text = this.names.de && this.names.de !== '' ? this.names.de : this.refGame.global.resources.getOtherText('noNameFor', {
            lang: this.refGame.global.resources.getOtherText('german')
        });
        this.pages.name.en.text = this.names.en && this.names.en !== '' ? this.names.en : this.refGame.global.resources.getOtherText('noNameFor', {
            lang: this.refGame.global.resources.getOtherText('english')
        });
        this.pages.name.it.text = this.names.it && this.names.it !== '' ? this.names.it : this.refGame.global.resources.getOtherText('noNameFor', {
            lang: this.refGame.global.resources.getOtherText('italien')
        });

    }

    reset() {
        this.currentTutorialText = "";
        this.refGame.global.util.setTutorialText('');
        this.pages.options.showLastName.select(this.pages.options.showLastName.isChecked());
        this.pages.options.showFirstName.select(this.pages.options.showFirstName.isChecked());
        this.pages.options.showAge.select(this.pages.options.showAge.isChecked());
        this.pages.options.showBirthdate.select(this.pages.options.showBirthdate.isChecked());
        this.pages.options.showDeathdate.select(this.pages.options.showDeathdate.isChecked());
        this.pages.options.showParents.select(this.pages.options.showParents.isChecked());
        this.pages.options.showAddress.select(this.pages.options.showAddress.isChecked());
        this.pages.options.showChild.select(this.pages.options.showChild.isChecked());
        this.tree.reset();
        this.options = {};
        this.intruders = [];
        this.createdCharacter = [];
        this.characters = {
            me: ['c_m_1', 'i_m_2'],
            parents: {
                men: ['i_m_1', 'a_m_1'],
                women: ['a_w_1']
            }, grandParents: {
                men: ['o_m_1', 'o_m_2', 'o_m_3', 'o_m_4', 'o_m_5', 'o_m_6', 'i_m_3'],
                women: ['o_w_1', 'o_w_2', 'o_w_3', 'o_w_4', 'o_w_5', 'o_w_6', 'i_w_1']
            }
        };
        this.intrudersList = [
            'o_w_1',
            'o_w_2',
            'o_w_3',
            'o_w_4',
            'o_w_5',
            'o_w_6',
            'i_w_1',
            'o_m_1',
            'o_m_2',
            'o_m_3',
            'o_m_4',
            'o_m_5',
            'o_m_6',
            'i_m_3',
            'i_m_1',
            'a_m_1',
            'a_w_1',
            'c_m_1',
            'i_m_2'
        ];
    }

    showTree() {
        this.hideAll();
        this.tree.setVisible(true);
        this.nextBtn.setVisible(true);
        this.addIntruderBtn.setVisible(true);
        this.currentTutorialText = "create";
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentTutorialText));
    }


    showTitle() {
        this.hideAll();
        this.mainTitle.visible = true;
        this.startButton.setVisible(true);
    }

    generateTree(degree) {
        this.createdCharacter.push([{}]);
        this.createdCharacter.push([{}, {}]);
        this.createdCharacter.push([{}, {}, {}, {}]);

        if (degree > 3) {
            this.createdCharacter.push([{}, {}, {}, {}, {}, {}, {}, {}]);
            this.tree.addEmptyCell(50, 100, 50, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
                gender: 'women',
                age: 'greatGrandParents',
                tablePosX: 0,
                tablePosY: 3
            });
            this.tree.addEmptyCell(105, 100, 50, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
                gender: 'men',
                age: 'greatGrandParents',
                tablePosX: 1,
                tablePosY: 3
            });

            this.tree.addEmptyCell(185, 100, 50, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
                gender: 'women',
                age: 'greatGrandParents',
                tablePosX: 2,
                tablePosY: 3
            });
            this.tree.addEmptyCell(240, 100, 50, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
                gender: 'men',
                age: 'greatGrandParents',
                tablePosX: 3,
                tablePosY: 3
            });

            this.tree.addEmptyCell(360, 100, 50, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
                gender: 'women',
                age: 'greatGrandParents',
                tablePosX: 4,
                tablePosY: 3
            });
            this.tree.addEmptyCell(415, 100, 50, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
                gender: 'men',
                age: 'greatGrandParents',
                tablePosX: 5,
                tablePosY: 3
            });

            this.tree.addEmptyCell(495, 100, 50, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
                gender: 'women',
                age: 'greatGrandParents',
                tablePosX: 6,
                tablePosY: 3
            });
            this.tree.addEmptyCell(550, 100, 50, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
                gender: 'men',
                age: 'greatGrandParents',
                tablePosX: 7,
                tablePosY: 3
            });

        }

        //Second line
        this.tree.addEmptyCell(degree > 3 ? 118 : 113, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
            gender: 'women',
            age: 'grandParents',
            tablePosX: 0,
            tablePosY: 2
        });
        this.tree.addEmptyCell(degree > 3 ? 173 : 178, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
            gender: 'men',
            age: 'grandParents',
            tablePosX: 1,
            tablePosY: 2
        });

        this.tree.addEmptyCell(degree > 3 ? 427 : 422, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
            gender: 'women',
            age: 'grandParents',
            tablePosX: 2,
            tablePosY: 2
        });
        this.tree.addEmptyCell(degree > 3 ? 482 : 487, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
            gender: 'men',
            age: 'grandParents',
            tablePosX: 3,
            tablePosY: 2
        });

        //Third line
        this.tree.addEmptyCell(degree > 3 ? 230 : 225, degree > 3 ? 300 : 250, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'), this.onCellClick.bind(this), {
            gender: 'women',
            age: 'parents',
            tablePosX: 0,
            tablePosY: 1
        });
        this.tree.addEmptyCell(degree > 3 ? 370 : 375, degree > 3 ? 300 : 250, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'), this.onCellClick.bind(this), {
            gender: 'men',
            age: 'parents',
            tablePosX: 1,
            tablePosY: 1
        });

        //Fourth line
        this.tree.addEmptyCell(300, 400, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cell'), this.onCellClick.bind(this), {
            age: 'me',
            tablePosX: 0,
            tablePosY: 0
        });
    }

    onCellClick(cell) {
        this.askForCharacter(cell, null, false);
    }

    askForCharacter(cell, infos) {
        infos = infos
            ? infos
            : (this.createdCharacter[cell.cellInfo.tablePosY][cell.cellInfo.tablePosX].birthdate
                ? this.createdCharacter[cell.cellInfo.tablePosY][cell.cellInfo.tablePosX]
                : undefined);

        Swal.fire({
            title: this.refGame.global.resources.getOtherText('addCharacter'),
            width: 1000,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK',
            cancelButtonText: this.refGame.global.resources.getOtherText('cancel'),
            html:
                '<div style="text-align: left">' +
                '<form>' +
                '  <div class="row">' +
                '<div class="col-6"> ' +
                '<div class="form-group row">' +
                '    <label for="firstname" class="col-3 col-form-label">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="firstname" name="firstname" type="text" required="required" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="lastname" class="col-3 col-form-label">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="lastname" name="lastname" value="" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6">  ' +
                '<div class="form-group row">' +
                '    <label for="birthdate" class="col-5 col-form-label">' + this.refGame.global.resources.getOtherText('birthdate', {param: ''}) + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="birthdate" name="bithdate" type="date" value="" class="form-control"> ' +
                '        <div class="input-group-append">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('age', {param: ''}) + '&nbsp;' + '<span id="age"></span></div>' +
                '        </div>' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="deathdate" class="col-5 col-form-label">' + this.refGame.global.resources.getOtherText('deathdate', {param: ''}) + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="deathdate" name="deathdate" type="date" value="" class="form-control"> ' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '  <div class="form-group row">' +
                '    <label for="address" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('address', {param: ''}) + '</label> ' +
                '    <div class="col-10">' +
                '      <textarea id="address" name="address" cols="40" rows="2"  value="" class="form-control"></textarea>' +
                '    </div>' +
                '  </div>' +
                ((cell.cellInfo.age === 'greatGrandParents' || (this.refGame.global.resources.getDegre() < 4 && cell.cellInfo.age === 'grandParents')) ?
                        ('  <div class="form-group row">' +
                            '    <label for="motherLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('mother') + '</label> ' +
                            '    <div class="col-10">' +
                            '      <div class="input-group">' +
                            '        <div class="input-group-prepend">' +
                            '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                            '        </div> ' +
                            '        <input id="motherLastName" name="motherLastName" placeholder="" type="text" value="" class="form-control">' +
                            '        <div class="input-group-prepend">' +
                            '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                            '        </div> ' +
                            '        <input id="motherFirstName" name="motherFirstName" placeholder="" type="text"  value="" class="form-control">' +
                            '      </div>' +
                            '    </div>' +
                            '  </div>' +
                            '  <div class="form-group row">' +
                            '    <label for="fatherLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('father') + '</label> ' +
                            '    <div class="col-10">' +
                            '      <div class="input-group">' +
                            '        <div class="input-group-prepend">' +
                            '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                            '        </div> ' +
                            '        <input id="fatherLastName" name="fatherLastName" placeholder="" type="text" class="form-control" value="">' +
                            '        <div class="input-group-prepend">' +
                            '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                            '        </div> ' +
                            '        <input id="fatherFirstName" name="fatherFirstName" placeholder="" type="text" class="form-control" value="">' +
                            '      </div>' +
                            '    </div>' +
                            '  </div>')
                        : ''
                ) +
                '  <div class="form-group row">' +
                '    <label for="text" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('character') + '</label> ' +
                '    <div class="col-10">' +
                '       <div id="selector"></div>' +
                '    </div>' +
                '  </div>' +
                ' ' +
                '</form>' +
                '<input type="hidden" id="character"><br><br>' +
                '</div>',
            onBeforeOpen: function () {

                document.getElementById('birthdate').onchange = function () {
                    document.getElementById('age').innerHTML = this.calculateAge(document.getElementById('birthdate').value, document.getElementById('deathdate').value);
                }.bind(this);
                document.getElementById('deathdate').onchange = function () {
                    document.getElementById('age').innerHTML = this.calculateAge(document.getElementById('birthdate').value, document.getElementById('deathdate').value);
                }.bind(this);


                let icons = [];
                let gender = cell.cellInfo.gender;
                let age = cell.cellInfo.age;
                age = age === 'greatGrandParents' ? 'grandParents' : age;
                if (gender) {
                    for (let char of this.characters[age][gender]) {
                        for (let intruder of this.intruders) {
                            if (intruder.getInfo().image === char)
                                continue;
                        }
                        icons.push({
                            iconFilePath: this.refGame.global.resources.getImage(char).image.src,
                            iconValue: char
                        });
                    }
                } else {
                    for (let char of this.characters[age]) {
                        for (let intruder of this.intruders) {
                            if (intruder.getInfo().image === char)
                                continue;
                        }
                        icons.push({
                            iconFilePath: this.refGame.global.resources.getImage(char).image.src,
                            iconValue: char
                        });
                    }
                }
                if (infos && infos.image) {
                    icons.splice(0, 0, {
                        iconFilePath: this.refGame.global.resources.getImage(infos.image).image.src,
                        iconValue: infos.image
                    });
                }

                let select = new IconSelect("selector", {
                    'selectedIconWidth': 'auto',
                    'selectedIconHeight': 70,
                    'selectedBoxPadding': 5,
                    'iconsWidth': 'auto',
                    'iconsHeight': 70,
                    'boxWidth': 32,
                    'boxHeight': 75,
                    'boxIconSpace': 3,
                    'vectoralIconNumber': (icons.length <= 15 ? icons.length : 15),
                    'horizontalIconNumber': (icons.length <= 15 ? 1 : (Math.floor(icons.length / 15) + 1))
                });
                select.refresh(icons);

                if (infos && infos.image) {
                    select.setSelectedValue(infos.image);
                }

                document.getElementById('selector').addEventListener('changed', function (e) {
                    document.getElementById('character').value = select.getSelectedValue();
                });

                if (infos) {
                    $('#character').val(infos.image);
                    $('#firstname').val(infos.firstName);
                    $('#lastname').val(infos.lastName);
                    $('#birthdate').val(this.stringToDate(infos.birthdate));
                    $('#deathdate').val(this.stringToDate(infos.deathdate));
                    $('#address').val(infos.address.replace(", ", "\n"));
                    if ($('#fatherFirstName').length) $('#fatherFirstName').val(infos.father.firstName);
                    if ($('#fatherLastName').length) $('#fatherLastName').val(infos.father.lastName);
                    if ($('#motherFirstName').length) $('#motherFirstName').val(infos.mother.firstName);
                    if ($('#motherLastName').length) $('#motherLastName').val(infos.mother.lastName);
                }

            }.bind(this),
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#character').val(),
                        $('#firstname').val(),
                        $('#lastname').val(),
                        $('#birthdate').val(),
                        $('#deathdate').val(),
                        $('#address').val(),
                        $('#fatherFirstName').length ? $('#fatherFirstName').val() : '',
                        $('#fatherLastName').length ? $('#fatherLastName').val() : '',
                        $('#motherFirstName').length ? $('#motherFirstName').val() : '',
                        $('#motherLastName').length ? $('#motherLastName').val() : '',
                    ])
                })
            }
        }).then(function (result) {
            if (result && result.value && result.value.length === 10) {
                let character = {
                    lastName: result.value[2].trim(),
                    firstName: result.value[1].trim(),
                    birthdate: this.dateToString(result.value[3].trim()),
                    deathdate: this.dateToString(result.value[4].trim()),
                    address: result.value[5].replace("\n", ", ").trim(),
                    image: result.value[0].trim(),
                    mother: {
                        firstName: result.value[8].trim(),
                        lastName: result.value[9].trim()
                    },
                    father: {
                        firstName: result.value[6].trim(),
                        lastName: result.value[7].trim()
                    },
                    child: {}
                };
                let checkResult = this.checkForError(cell, character, false);
                if (checkResult.code === 'OK') {
                    cell.setSprite(this.refGame.global.resources.getImage(character.image), character, this.askForCharacter.bind(this));
                    cell.onClick = undefined;
                    this.removeCharacterFromList(infos, character.image, cell.cellInfo.gender, cell.cellInfo.age);
                    this.createdCharacter[cell.cellInfo.tablePosY][cell.cellInfo.tablePosX] = character;
                    this.tree.display();
                } else {
                    Swal.fire(
                        this.refGame.global.resources.getOtherText('error'),
                        checkResult.error, "error").then(function () {
                        this.askForCharacter(cell, character);
                    }.bind(this));
                }
            }
        }.bind(this));
    }

    removeCharacterFromList(infos, image, gender, age) {
        this.intrudersList.splice(this.intrudersList.indexOf(image), 1);
        age = age === 'greatGrandParents' ? 'grandParents' : age;
        if (gender) {
            this.characters[age][gender].splice(this.characters[age][gender].indexOf(image), 1);
        } else {
            this.characters[age].splice(this.characters[age].indexOf(image), 1);
        }
        //Add old image to array
        if (infos && infos.image !== image) {
            this.intrudersList.push(infos.image);
            if (gender) {
                this.characters[age][gender].splice(0, 0, infos.image);
            } else {
                this.characters[age].splice(0, 0, infos.image);
            }
        }
    }

    checkForError(cell, character, isIntruder = false) {
        let displayParent = isIntruder ? true : (cell.cellInfo.age === 'greatGrandParents' || (this.refGame.global.resources.getDegre() < 4 && cell.cellInfo.age === 'grandParents'));

        let checkResult = {
            code: 'OK',
            error: ''
        };
        if (!(character.lastName && character.lastName !== ''
            && character.firstName && character.firstName !== ''
            && character.birthdate && character.birthdate !== ''
            && character.address && character.address !== ''
            && character.image && character.image !== ''
            && (!displayParent ^ (character.mother.firstName && character.mother.firstName !== ''
                && character.mother.lastName && character.mother.lastName !== ''
                && character.father.firstName && character.father.firstName !== ''
                && character.father.lastName && character.father.lastName !== ''))
            && !(isIntruder ^ (character.child.firstName && character.child.firstName !== ''
                && character.child.lastName && character.child.lastName !== '')))) {
            checkResult.code = 'error';
            checkResult.error = 'informationsMissing';
        } else {
            if (!displayParent) {
                let father = this.createdCharacter[cell.cellInfo.tablePosY + 1][cell.cellInfo.tablePosX * 2 + 1];
                let mother = this.createdCharacter[cell.cellInfo.tablePosY + 1][cell.cellInfo.tablePosX * 2];
                //test mother
                if (mother.birthdate && (this.calculateAge(mother.birthdate, '') < this.calculateAge(character.birthdate, '') + 10)) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooOldForMother';
                }
                //test father
                else if (father.birthdate && (this.calculateAge(father.birthdate, '') < this.calculateAge(character.birthdate, '') + 10)) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooOldForFather';
                }
            }
            if (!isIntruder && cell.cellInfo.tablePosY > 0) {
                let child = this.createdCharacter[cell.cellInfo.tablePosY - 1][Math.floor(cell.cellInfo.tablePosX / 2)];
                if (child.birthdate && (this.calculateAge(child.birthdate, '') > this.calculateAge(character.birthdate, '') - 10)) {
                    checkResult.code = 'error';
                    checkResult.error = 'tooYoungForChild';
                }
            }
        }
        checkResult.error = checkResult.code !== 'OK' ? this.refGame.global.resources.getOtherText(checkResult.error) : '';
        return checkResult;
    }

    calculateAge(birthDateString, deathDateString) {
        birthDateString = birthDateString.split('.').reverse().join('-');
        let birthdate = new Date(birthDateString);
        let age;
        let month;
        if (deathDateString && deathDateString !== "") {
            deathDateString = deathDateString.split('.').reverse().join('-');
            let deathdate = new Date(deathDateString);
            age = deathdate.getFullYear() - birthdate.getFullYear();
            month = deathdate.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && deathdate.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return (age < 0 || isNaN(age)) ? 0 : age;
        } else {
            let today = new Date();
            age = today.getFullYear() - birthdate.getFullYear();
            month = today.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return (age < 0 || isNaN(age)) ? 0 : age;
        }
    }

    dateToString(date) {
        if (!date || date === '')
            return '';
        return date.split('-').reverse().join('.');
    }

    stringToDate(string) {
        if (!string || string === '')
            return '';
        return string.split('.').reverse().join('-');
    }

    endInsertion() {
        if (this.names.fr && this.names.fr !== '' && this.names.de && this.names.de !== '') {
            this.hideAll();
            this.pages.submission.btnSubmit.setVisible(true);
            this.pages.submission.btnDelete.setVisible(true);
            this.pages.submission.btnTest.setVisible(true);
            this.pages.submission.submissionTitle.visible = true;
            this.pages.submission.backToName.setVisible(true);
        } else {
            Swal.fire(
                this.refGame.global.resources.getOtherText('warning'),
                this.refGame.global.resources.getOtherText('noTranslation'),
                'warning');
        }
    }

    handleDeleteExercice() {
        Swal.fire({
            title: this.refGame.global.resources.getOtherText('confirmDeletion'),
            text: this.refGame.global.resources.getOtherText('confirmDeletionText'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33d33',
            confirmButtonText: this.refGame.global.resources.getOtherText('yes'),
            cancelButtonText: this.refGame.global.resources.getOtherText('no')
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    this.refGame.global.resources.getOtherText('deleted'),
                    this.refGame.global.resources.getOtherText('deletedExerciceText'),
                    'success'
                );
                this.show();
            }
        });
    }

    handleTestExercice() {
        let exercice = this.buildExercice();
        this.refGame.trainMode.init(false, 'createMode');
        this.refGame.trainMode.show(exercice);
    }

    handleSubmitExercice() {
        let exercice = this.buildExercice();
        console.log(exercice);
        console.log(this.intruders)
        this.refGame.global.resources.saveExercice(JSON.stringify(exercice), function (result) {
            Swal.fire(
                result.message,
                undefined,
                result.type
            );
            this.show();
        }.bind(this));
    }

    buildExercice() {
        let exercice = {
            scenario: {},
            options: this.options,
            name: this.names
        };
        exercice.scenario.intrus = [];
        exercice.options.numberOfIntruder = this.intruders.length;

        for (let intruder of this.intruders) {
            let intruderInfo = intruder.getInfo();
            exercice.scenario.intrus.push(intruderInfo);
        }

        //Level 1
        exercice.scenario.enfant = this.createdCharacter[0][0];

        //Level 2
        exercice.scenario.enfant.mother = this.createdCharacter[1][0];
        exercice.scenario.enfant.father = this.createdCharacter[1][1];

        //Level 3
        exercice.scenario.enfant.mother.mother = this.createdCharacter[2][0];
        exercice.scenario.enfant.mother.father = this.createdCharacter[2][1];
        exercice.scenario.enfant.father.mother = this.createdCharacter[2][2];
        exercice.scenario.enfant.father.father = this.createdCharacter[2][3];

        //Level 4 if exists
        if (this.refGame.global.resources.getDegre() > 3) {
            exercice.scenario.enfant.mother.mother.mother = this.createdCharacter[3][0];
            exercice.scenario.enfant.mother.mother.father = this.createdCharacter[3][1];
            exercice.scenario.enfant.mother.father.mother = this.createdCharacter[3][2];
            exercice.scenario.enfant.mother.father.father = this.createdCharacter[3][3];
            exercice.scenario.enfant.father.mother.mother = this.createdCharacter[3][4];
            exercice.scenario.enfant.father.mother.father = this.createdCharacter[3][5];
            exercice.scenario.enfant.father.father.mother = this.createdCharacter[3][6];
            exercice.scenario.enfant.father.father.father = this.createdCharacter[3][7];
        }

        return exercice;
    }

    showOptions() {
        let total = 0, created = 0;
        for (let level of this.createdCharacter) {
            for (let character of level) {
                total++;
                if (character.birthdate)
                    created++;
            }
        }
        if (created < total) {
            Swal.fire(
                this.refGame.global.resources.getOtherText('warning'),
                this.refGame.global.resources.getOtherText('notFull'),
                'warning');
        } else {
            this.currentTutorialText = "options";
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentTutorialText));
            this.hideAll();
            this.nameButton.setVisible(true);
            this.pages.options.optionsTitle.visible = true;
            this.pages.options.showLastName.setVisible(true);
            this.pages.options.showFirstName.setVisible(true);
            this.pages.options.showAge.setVisible(true);
            this.pages.options.showBirthdate.setVisible(true);
            this.pages.options.showDeathdate.setVisible(true);
            this.pages.options.showParents.setVisible(true);
            this.pages.options.showAddress.setVisible(true);
            this.pages.options.showChild.setVisible(true);
            this.pages.options.backToTree.setVisible(true);
        }
    }


    hideAll() {
        this.nextBtn.setVisible(false);
        this.endButton.setVisible(false);
        this.tree.setVisible(false);
        this.mainTitle.visible = false;
        this.startButton.setVisible(false);
        this.pages.submission.btnSubmit.setVisible(false);
        this.pages.submission.btnDelete.setVisible(false);
        this.pages.submission.btnTest.setVisible(false);
        this.pages.submission.submissionTitle.visible = false;
        this.pages.submission.backToName.setVisible(false);
        this.pages.options.optionsTitle.visible = false;
        this.pages.options.showLastName.setVisible(false);
        this.pages.options.showFirstName.setVisible(false);
        this.pages.options.showAge.setVisible(false);
        this.pages.options.showBirthdate.setVisible(false);
        this.pages.options.showDeathdate.setVisible(false);
        this.pages.options.showParents.setVisible(false);
        this.pages.options.showAddress.setVisible(false);
        this.pages.options.showChild.setVisible(false);
        this.pages.options.backToTree.setVisible(false);
        this.addIntruderBtn.setVisible(false);
        this.editNameButton.setVisible(false);
        this.nameButton.setVisible(false);
        this.pages.name.fr.visible = false;
        this.pages.name.de.visible = false;
        this.pages.name.en.visible = false;
        this.pages.name.it.visible = false;
        this.pages.name.backToOptions.setVisible(false);

    }


    addIntruder(character) {
        let total = 0, created = 0;
        for (let level of this.createdCharacter) {
            for (let character of level) {
                total++;
                if (character.birthdate)
                    created++;
            }
        }
        if (total !== created) {
            Swal.fire(
                this.refGame.global.resources.getOtherText('warning'),
                this.refGame.global.resources.getOtherText('notFull'),
                'warning');
        } else
            this.askForIntruder(character, character ? character.getInfo() : undefined);
    }

    askForIntruder(cell, infos) {
        Swal.fire({
            title: this.refGame.global.resources.getOtherText('addCharacter'),
            width: 1000,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK',
            cancelButtonText: this.refGame.global.resources.getOtherText('cancel'),
            html:
                '<div style="text-align: left">' +
                '<form>' +
                '  <div class="row">' +
                '<div class="col-6"> ' +
                '<div class="form-group row">' +
                '    <label for="firstname" class="col-3 col-form-label">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="firstname" name="firstname" type="text" required="required" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="lastname" class="col-3 col-form-label">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</label> ' +
                '    <div class="col-9">' +
                '      <input id="lastname" name="lastname" value="" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-6">  ' +
                '<div class="form-group row">' +
                '    <label for="birthdate" class="col-5 col-form-label">' + this.refGame.global.resources.getOtherText('birthdate', {param: ''}) + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="birthdate" name="bithdate" type="date" value="" class="form-control"> ' +
                '        <div class="input-group-append">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('age', {param: ''}) + '&nbsp;' + '<span id="age"></span></div>' +
                '        </div>' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '<div class="col-6">' +
                '  <div class="form-group row">' +
                '    <label for="deathdate" class="col-5 col-form-label">' + this.refGame.global.resources.getOtherText('deathdate', {param: ''}) + '</label> ' +
                '    <div class="col-7">' +
                '      <div class="input-group">' +
                '        <input id="deathdate" name="deathdate" type="date" value="" class="form-control"> ' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>' +
                '</div>' +
                '  <div class="form-group row">' +
                '    <label for="address" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('address', {param: ''}) + '</label> ' +
                '    <div class="col-10">' +
                '      <textarea id="address" name="address" cols="40" rows="2"  value="" class="form-control"></textarea>' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="motherLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('mother') + '</label> ' +
                '    <div class="col-10">' +
                '      <div class="input-group">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="motherLastName" name="motherLastName" placeholder="" type="text" value="" class="form-control">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="motherFirstName" name="motherFirstName" placeholder="" type="text"  value="" class="form-control">' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="fatherLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('father') + '</label> ' +
                '    <div class="col-10">' +
                '      <div class="input-group">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="fatherLastName" name="fatherLastName" placeholder="" type="text" class="form-control" value="">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="fatherFirstName" name="fatherFirstName" placeholder="" type="text" class="form-control" value="">' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="childLastName" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('child',{param: ''}) + '</label> ' +
                '    <div class="col-10">' +
                '      <div class="input-group">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('lastName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="childLastName" name="childLastName" placeholder="" type="text" class="form-control" value="">' +
                '        <div class="input-group-prepend">' +
                '          <div class="input-group-text">' + this.refGame.global.resources.getOtherText('firstName', {param: ''}) + '</div>' +
                '        </div> ' +
                '        <input id="childFirstName" name="childFirstName" placeholder="" type="text" class="form-control" value="">' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="text" class="col-2 col-form-label">' + this.refGame.global.resources.getOtherText('character') + '</label> ' +
                '    <div class="col-10">' +
                '       <div id="selector"></div>' +
                '    </div>' +
                '  </div>' +
                '</form>' +
                '<input type="hidden" id="character"><br><br>' +
                '</div>',
            onBeforeOpen: function () {

                document.getElementById('birthdate').onchange = function () {
                    document.getElementById('age').innerHTML = this.calculateAge(document.getElementById('birthdate').value, document.getElementById('deathdate').value);
                }.bind(this);
                document.getElementById('deathdate').onchange = function () {
                    document.getElementById('age').innerHTML = this.calculateAge(document.getElementById('birthdate').value, document.getElementById('deathdate').value);
                }.bind(this);

                let icons = [];
                for (let char of this.intrudersList) {
                    icons.push({
                        iconFilePath: this.refGame.global.resources.getImage(char).image.src,
                        iconValue: char
                    });
                }
                if (infos && infos.image) {
                    icons.push({
                        iconFilePath: this.refGame.global.resources.getImage(infos.image).image.src,
                        iconValue: infos.image
                    });
                }

                let select = new IconSelect("selector", {
                    'selectedIconWidth': 'auto',
                    'selectedIconHeight': 70,
                    'selectedBoxPadding': 5,
                    'iconsWidth': 'auto',
                    'iconsHeight': 70,
                    'boxWidth': 32,
                    'boxHeight': 75,
                    'boxIconSpace': 3,
                    'vectoralIconNumber': (icons.length <= 15 ? icons.length : 15),
                    'horizontalIconNumber': (icons.length <= 15 ? 1 : (Math.floor(icons.length / 15) + 1))
                });

                select.refresh(icons);

                if (infos && infos.image) {
                    select.setSelectedValue(infos.image);
                }

                document.getElementById('selector').addEventListener('changed', function (e) {
                    document.getElementById('character').value = select.getSelectedValue();
                });

                if (infos) {
                    $('#character').val(infos.image);
                    $('#firstname').val(infos.firstName);
                    $('#lastname').val(infos.lastName);
                    $('#birthdate').val(this.stringToDate(infos.birthdate));
                    $('#deathdate').val(this.stringToDate(infos.deathdate));
                    $('#address').val(infos.address.replace(", ", "\n"));
                    if ($('#fatherFirstName').length) $('#fatherFirstName').val(infos.father.firstName);
                    if ($('#fatherLastName').length) $('#fatherLastName').val(infos.father.lastName);
                    if ($('#motherFirstName').length) $('#motherFirstName').val(infos.mother.firstName);
                    if ($('#motherLastName').length) $('#motherLastName').val(infos.mother.lastName);
                    if ($('#childFirstName').length) $('#childFirstName').val(infos.child.firstName);
                    if ($('#childLastName').length) $('#childLastName').val(infos.child.lastName);
                }

            }.bind(this),
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#character').val(),
                        $('#firstname').val(),
                        $('#lastname').val(),
                        $('#birthdate').val(),
                        $('#deathdate').val(),
                        $('#address').val(),
                        $('#fatherFirstName').length ? $('#fatherFirstName').val() : '',
                        $('#fatherLastName').length ? $('#fatherLastName').val() : '',
                        $('#motherFirstName').length ? $('#motherFirstName').val() : '',
                        $('#motherLastName').length ? $('#motherLastName').val() : '',
                        $('#childFirstName').length ? $('#childFirstName').val() : '',
                        $('#childLastName').length ? $('#childLastName').val() : '',
                    ])
                })
            }
        }).then(function (result) {
            if (result && result.value && result.value.length === 12) {
                let character = {
                    lastName: result.value[2].trim(),
                    firstName: result.value[1].trim(),
                    birthdate: this.dateToString(result.value[3].trim()),
                    deathdate: this.dateToString(result.value[4].trim()),
                    address: result.value[5].replace("\n", ", ").trim(),
                    image: result.value[0].trim(),
                    mother: {
                        firstName: result.value[8].trim(),
                        lastName: result.value[9].trim()
                    },
                    father: {
                        firstName: result.value[6].trim(),
                        lastName: result.value[7].trim()
                    },
                    child: {
                        firstName: result.value[10].trim(),
                        lastName: result.value[11].trim()
                    }
                };
                let checkResult = this.checkForError(cell, character, true);
                if (checkResult.code === 'OK') {
                    this.tree.intruders = [];
                    if (this.intruders.indexOf(cell) > -1) {
                        this.intruders.slice(this.intruders.indexOf(cell), 1);
                        cell = this.intruders[this.intruders.indexOf(cell)];
                        cell = new Character(30 + cell.getX(), cell.getY(), 22, 65, this.refGame.global.resources.getImage(character.image), character, undefined, this.addIntruder.bind(this));
                        cell.setCanMove(false);
                        this.intruders[this.intruders.indexOf(cell)] = cell;
                        if (character.image !== infos.image) {
                            this.intrudersList.push(infos.image);
                        }
                    } else {
                        let x = 20;
                        let y = 530;
                        if (this.intruders.length > 0) {
                            x = this.intruders[this.intruders.length - 1].getX();
                        }
                        let char = new Character(30 + x, y, 22, 65, this.refGame.global.resources.getImage(character.image), character, undefined, this.addIntruder.bind(this));
                        char.setCanMove(false);
                        this.intruders.push(char);
                    }
                    for (let char of this.intruders) {
                        this.tree.addCharacter(char, true);
                    }
                    this.tree.display();
                    this.intrudersList.splice(this.intrudersList.indexOf(character.image), 1);
                    console.log("OKKKKKKKKKK");
                } else {
                    Swal.fire(
                        this.refGame.global.resources.getOtherText('error'),
                        checkResult.error, "error").then(function () {
                        this.askForIntruder(cell, character);
                    }.bind(this));
                }
            }
        }.bind(this));
    }

    editName() {

        Swal.fire({
            title: this.refGame.global.resources.getOtherText('chooseName'),
            html: '<form>' +
                '  <div class="form-group row">' +
                '    <label for="fr" class="col-4 col-form-label">' + this.refGame.global.resources.getOtherText('french') + '</label> ' +
                '    <div class="col-8">' +
                '      <input id="frName" name="fr" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="de" class="col-4 col-form-label">' + this.refGame.global.resources.getOtherText('german') + '</label> ' +
                '    <div class="col-8">' +
                '      <input id="deName" name="de" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="en" class="col-4 col-form-label">' + this.refGame.global.resources.getOtherText('english') + '</label> ' +
                '    <div class="col-8">' +
                '      <input id="enName" name="en" type="text" class="form-control">' +
                '    </div>' +
                '  </div>' +
                '  <div class="form-group row">' +
                '    <label for="it" class="col-4 col-form-label">' + this.refGame.global.resources.getOtherText('italien') + '</label> ' +
                '    <div class="col-8">' +
                '      <input id="itName" name="it" type="text" class="form-control">' +
                '    </div>' +
                '  </div> ' +
                '</form>',
            confirmButtonText: 'OK',
            onBeforeOpen: function () {
                document.getElementById('frName').value = this.names.fr;
                document.getElementById('deName').value = this.names.de;
                document.getElementById('enName').value = this.names.en;
                document.getElementById('itName').value = this.names.it;
            }.bind(this),
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#frName').val(),
                        $('#deName').val(),
                        $('#enName').val(),
                        $('#itName').val()
                    ])
                })
            }
        }).then(function (result) {
            if (result && result.value && result.value.length === 4) {
                this.names.fr = result.value[0];
                this.names.de = result.value[1];
                this.names.en = result.value[2];
                this.names.it = result.value[3];
                this.pages.name.fr.text = this.names.fr && this.names.fr !== '' ? this.names.fr : this.refGame.global.resources.getOtherText('noNameFor', {
                    lang: this.refGame.global.resources.getOtherText('french')
                });
                this.pages.name.de.text = this.names.de && this.names.de !== '' ? this.names.de : this.refGame.global.resources.getOtherText('noNameFor', {
                    lang: this.refGame.global.resources.getOtherText('german')
                });
                this.pages.name.en.text = this.names.en && this.names.en !== '' ? this.names.en : this.refGame.global.resources.getOtherText('noNameFor', {
                    lang: this.refGame.global.resources.getOtherText('english')
                });
                this.pages.name.it.text = this.names.it && this.names.it !== '' ? this.names.it : this.refGame.global.resources.getOtherText('noNameFor', {
                    lang: this.refGame.global.resources.getOtherText('italien')
                });
            }
        }.bind(this));

    }

    displayName() {
        this.currentTutorialText = "name";
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentTutorialText));
        let count = 0;
        if (this.pages.options.showAddress.isChecked()) count++;
        if (this.pages.options.showAge.isChecked()) count++;
        if (this.pages.options.showBirthdate.isChecked()) count++;
        if (this.pages.options.showChild.isChecked()) count++;
        if (this.pages.options.showDeathdate.isChecked()) count++;
        if (this.pages.options.showFirstName.isChecked()) count++;
        if (this.pages.options.showLastName.isChecked()) count++;
        if (this.pages.options.showParents.isChecked()) count++;

        if (count >= 4) {
            this.hideAll();
            this.pages.name.fr.visible = true;
            this.pages.name.de.visible = true;
            this.pages.name.en.visible = true;
            this.pages.name.it.visible = true;
            this.editNameButton.setVisible(true);
            this.pages.name.backToOptions.setVisible(true);
            this.endButton.setVisible(true);
            this.options = {
                showLastName: this.pages.options.showLastName.isChecked(),
                showFirstName: this.pages.options.showFirstName.isChecked(),
                showAge: this.pages.options.showAge.isChecked(),
                showBirthdate: this.pages.options.showBirthdate.isChecked(),
                showDeathdate: this.pages.options.showDeathdate.isChecked(),
                showAddress: this.pages.options.showAddress.isChecked(),
                showParents: this.pages.options.showParents.isChecked(),
                showChild: this.pages.options.showChild.isChecked()
            }
        } else {
            Swal.fire(
                this.refGame.global.resources.getOtherText('warning'),
                this.refGame.global.resources.getOtherText('optionsNotCompleted'),
                'warning'
            );
        }
    }
}
/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */
class Explore extends Interface {


    constructor(refGame) {

        super(refGame, "explore");
        this.refGame = refGame;
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        this.clear();
        let scenario = this.refGame.global.resources.getScenario();
        this.generateTree(JSON.parse(scenario));
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('explore'));
    }

    generateTree(scenario) {
        let tree = new Tree(0, 0, 600, 600, this.refGame.global.resources.getImage('tree'));
        this.elements.push(tree);

        //SETUP CHILDREN
        scenario.enfant.mother.mother.mother.child = scenario.enfant.mother.mother.firstName + " " + scenario.enfant.mother.mother.lastName;
        scenario.enfant.mother.mother.father.child = scenario.enfant.mother.mother.firstName + " " + scenario.enfant.mother.mother.lastName;
        scenario.enfant.mother.father.mother.child = scenario.enfant.mother.father.firstName + " " + scenario.enfant.mother.father.lastName;
        scenario.enfant.mother.father.father.child = scenario.enfant.mother.father.firstName + " " + scenario.enfant.mother.father.lastName;
        scenario.enfant.father.father.mother.child = scenario.enfant.father.father.firstName + " " + scenario.enfant.father.father.lastName;
        scenario.enfant.father.father.father.child = scenario.enfant.father.father.firstName + " " + scenario.enfant.father.father.lastName;
        scenario.enfant.father.mother.mother.child = scenario.enfant.father.mother.firstName + " " + scenario.enfant.father.mother.lastName;
        scenario.enfant.father.mother.father.child = scenario.enfant.father.mother.firstName + " " + scenario.enfant.father.mother.lastName;
        scenario.enfant.mother.mother.child = scenario.enfant.mother.firstName + " " + scenario.enfant.mother.lastName;
        scenario.enfant.mother.father.child = scenario.enfant.mother.firstName + " " + scenario.enfant.mother.lastName;
        scenario.enfant.father.mother.child = scenario.enfant.father.firstName + " " + scenario.enfant.father.lastName;
        scenario.enfant.father.father.child = scenario.enfant.father.firstName + " " + scenario.enfant.father.lastName;
        scenario.enfant.mother.child = scenario.enfant.firstName + " " + scenario.enfant.lastName;
        scenario.enfant.father.child = scenario.enfant.firstName + " " + scenario.enfant.lastName;

        //First line
        tree.addCells(50, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.mother.mother.image), scenario.enfant.mother.mother.mother, this.displayInfo.bind(this));
        tree.addCells(105, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.mother.father.image), scenario.enfant.mother.mother.father, this.displayInfo.bind(this));

        tree.addCells(185, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.father.mother.image), scenario.enfant.mother.father.mother, this.displayInfo.bind(this));
        tree.addCells(240, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.father.father.image), scenario.enfant.mother.father.father, this.displayInfo.bind(this));

        tree.addCells(360, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.mother.mother.image), scenario.enfant.father.mother.mother, this.displayInfo.bind(this));
        tree.addCells(415, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.mother.father.image), scenario.enfant.father.mother.father, this.displayInfo.bind(this));

        tree.addCells(495, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.father.mother.image), scenario.enfant.father.father.mother, this.displayInfo.bind(this));
        tree.addCells(550, 100, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.father.father.image), scenario.enfant.father.father.father, this.displayInfo.bind(this));

        //Second line
        tree.addCells(118, 200, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.mother.image), scenario.enfant.mother.mother, this.displayInfo.bind(this));
        tree.addCells(173, 200, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.father.image), scenario.enfant.mother.father, this.displayInfo.bind(this));

        tree.addCells(427, 200, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.mother.image), scenario.enfant.father.mother, this.displayInfo.bind(this));
        tree.addCells(482, 200, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.father.image), scenario.enfant.father.father, this.displayInfo.bind(this));

        //Third line
        tree.addCells(230, 300, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.mother.image), scenario.enfant.mother, this.displayInfo.bind(this));
        tree.addCells(370, 300, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.father.image), scenario.enfant.father, this.displayInfo.bind(this));

        //Fourth line
        tree.addCells(300, 400, 50, 0, this.refGame.global.resources.getImage('cell'), this.refGame.global.resources.getImage(scenario.enfant.image), scenario.enfant, this.displayInfo.bind(this));
    }

    displayInfo(cell) {
        let personData = cell.sprite.info;
        let textToSpeech = this.refGame.global.resources.getOtherText('firstName', {param: personData.firstName}) + ";" +
            this.refGame.global.resources.getOtherText('lastName', {param: personData.lastName}) + " " + ";" +
            this.refGame.global.resources.getOtherText('age', {param: this.calculateAge(personData.birthdate, personData.deathdate)}) + " " + ";" +
            this.refGame.global.resources.getOtherText('birthdate', {param: personData.birthdate}) + " " + ";" +
            (personData.deathdate !== "" ? this.refGame.global.resources.getOtherText('deathdate', {param: personData.deathdate}) + " " : "") + ";" +
            this.refGame.global.resources.getOtherText('address', {param: personData.address}) + " " + ";" +
            this.refGame.global.resources.getOtherText('parents', {param: personData.father.firstName + " " + personData.father.lastName + " & " + personData.mother.firstName + " " + personData.mother.lastName}) + " " + ";" +
            (personData.child && personData.child !== "" ? this.refGame.global.resources.getOtherText('child', {param: personData.child}) : "");
        this.refGame.global.util.showAlertPersonnalized({
            title: personData.firstName + " " + personData.lastName,
            html: "<img src='" + this.refGame.global.resources.getImage(personData.image).getImage().src + "' style='width:20%;'><br>" +
                this.refGame.global.resources.getOtherText('firstName', {param: personData.firstName}) + "<br>" +
                this.refGame.global.resources.getOtherText('lastName', {param: personData.lastName}) + "<br>" +
                this.refGame.global.resources.getOtherText('age', {param: this.calculateAge(personData.birthdate, personData.deathdate)}) + "<br>" +
                this.refGame.global.resources.getOtherText('birthdate', {param: personData.birthdate}) + "<br>" +
                (personData.deathdate !== "" ? this.refGame.global.resources.getOtherText('deathdate', {param: personData.deathdate}) + "<br>" : "") +
                this.refGame.global.resources.getOtherText('address', {param: personData.address}) + "<br>" +
                this.refGame.global.resources.getOtherText('parents', {param: personData.father.firstName + " " + personData.father.lastName + " & " + personData.mother.firstName + " " + personData.mother.lastName}) + "<br>" +
                (personData.child && personData.child !== "" ? this.refGame.global.resources.getOtherText('child', {param: personData.child}) + "<br>" : "") +
                "<i class=\"fas fa-volume-up fa-3x\" id=\"speaker\" onclick=\"audio.textToSpeechFromTxt('" + textToSpeech + "', '" + this.refGame.global.resources.getLanguage() + "');\"></i>",
            text: ''
        });
    }

    calculateAge(birthDateString, deathDateString) {
        birthDateString = birthDateString.split('.').reverse().join('-');
        let birthdate = new Date(birthDateString);
        let age;
        let month;
        if (deathDateString !== "") {
            deathDateString = deathDateString.split('.').reverse().join('-');
            let deathdate = new Date(deathDateString);
            age = deathdate.getFullYear() - birthdate.getFullYear();
            month = deathdate.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && deathdate.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return age;
        } else {
            let today = new Date();
            age = today.getFullYear() - birthdate.getFullYear();
            month = today.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return age;
        }
    }

}
/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */
class Play extends Interface {

    /**
     * Constructeur de l'ihm de la création d'exercice
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {TrainMode} refTrainMode la référence au mode exercer pour tester les jeux
     */
    constructor(refGame, refTrainMode) {

        super(refGame, "play");

        this.exercices = {};

        this.selectedGameId = -1;

        this.refTrainMode = refTrainMode;

        this.elements = {
            title: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),
            scrollPane: new ScrollPane(0, 60, 600, 480),
            btn: new Button(300, 570, '', 0x0088FF, 0x000000, true)
        };
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 30;

        this.elements.btn.setOnClick(function () {
            if (this.selectedGameId < 0)
                return;
            this.playExerice(this.selectedGameId);
        }.bind(this));
    }

    init() {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        super.init();
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();

        this.exercices = this.refGame.global.resources.getExercicesEleves();
        if (this.exercices) this.exercices = JSON.parse(this.exercices);


        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.setElements(this.elements);

        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        this.generateScrollPane(lang);
        this.refGame.showText(this.refGame.global.resources.getTutorialText("play"));
        this.elements.btn.setText(this.refGame.global.resources.getOtherText('playBtn'));
    }


    playExerice(exId) {
        this.refGame.trainMode.init(false, 'playMode');
        this.refGame.trainMode.show(this.exercices[exId].exercice);
    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    updateFont(isOpendyslexic) {
        let font = isOpendyslexic ? 'Opendyslexic' : 'Arial';
    }

    generateScrollPane(lang) {
        if (!this.exercices)
            return;
        this.elements.scrollPane.clear();
        let tg = new ToggleGroup('single');
        for (let exID of Object.keys(this.exercices)) {
            let ex = this.exercices[exID];
            let label = (ex.exercice.name[lang] ? ex.exercice.name[lang] : '') + ex.name;
            let btn = new ToggleButton(0, 0, label, false, 580);
            btn.enable();
//            btn = new Button(0, 0, label, 0x00AA00, 0xFFFFFF, false, 580);

            tg.addButton(btn);

            btn.setOnClick(function () {
                this.selectedGameId = exID;
            }.bind(this));
            this.elements.scrollPane.addElements(btn);
        }
        this.elements.scrollPane.init();
    }
}
/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */
class Train extends Interface {


    constructor(refGame) {

        super(refGame, "train");
        this.refGame = refGame;

        this.exercice = {};
        this.evaluate = false;
        this.scenario = {};
        this.options = {};
        this.tree = new Tree(0, 0, 600, 600, this.refGame.global.resources.getImage('tree'));
        this.mainTitle = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
        this.mainTitle.anchor.set(0.5);
        this.mainTitle.x = 300;
        this.mainTitle.y = 150;
        this.startButton.setOnClick(this.startGame.bind(this));
        this.elements.push(this.tree);
        this.elements.push(this.mainTitle);
        this.elements.push(this.startButton);
        this.canMove = false;
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show(evaluate, exercice = undefined, interfaceCallback = undefined) {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        this.evaluate = evaluate;
        this.exercice = exercice;
        this.interfaceCallback = interfaceCallback;
        this.clear();
        if (!this.interfaceCallback)
            this.showTitle();
        else
            this.startGame();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
        this.setupSignalement();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('train'));
        this.startButton.setText(this.refGame.global.resources.getOtherText('startMode'));
        this.mainTitle.text = this.refGame.global.resources.getOtherText(this.evaluate ? 'evalModeTitle' : 'trainModeTitle');
    }

    startGame() {
        this.canMove = true;
        if (this.evaluate)
            this.refGame.global.statistics.addStats(2);

        this.tree.setVisible(true);
        this.mainTitle.visible = false;
        this.startButton.setVisible(false);
        let exercice = {};
        if (this.exercice)
            exercice = this.exercice;
        else {
            exercice = this.refGame.global.resources.getExercice();
            this.currentExerciceId = exercice.id;
            exercice = JSON.parse(exercice.exercice);
            this.refGame.inputs.signal.style.display = 'block';
        }

        this.scenario = exercice.scenario;
        this.options = exercice.options;
        this.tree.reset();
        this.generateTree(this.refGame.global.resources.getDegre());
    }


    showTitle() {
        this.tree.setVisible(false);
        this.mainTitle.visible = true;
        this.startButton.setVisible(true);
    }

    generateTree(degree) {
        let characters = [];

        let intruders = this.scenario.intrus ? this.scenario.intrus : JSON.parse(this.refGame.global.resources.getScenario()).intrus;

        for (let intruder of intruders) {
            intruder.child = intruder.child.firstName + " " + intruder.child.lastName;
        }

        //SETUP CHILDREN
        if (degree > 3) {
            this.scenario.enfant.mother.mother.mother.child = this.scenario.enfant.mother.mother.firstName + " " + this.scenario.enfant.mother.mother.lastName;
            this.scenario.enfant.mother.mother.father.child = this.scenario.enfant.mother.mother.firstName + " " + this.scenario.enfant.mother.mother.lastName;
            this.scenario.enfant.mother.father.mother.child = this.scenario.enfant.mother.father.firstName + " " + this.scenario.enfant.mother.father.lastName;
            this.scenario.enfant.mother.father.father.child = this.scenario.enfant.mother.father.firstName + " " + this.scenario.enfant.mother.father.lastName;
            this.scenario.enfant.father.father.mother.child = this.scenario.enfant.father.father.firstName + " " + this.scenario.enfant.father.father.lastName;
            this.scenario.enfant.father.father.father.child = this.scenario.enfant.father.father.firstName + " " + this.scenario.enfant.father.father.lastName;
            this.scenario.enfant.father.mother.mother.child = this.scenario.enfant.father.mother.firstName + " " + this.scenario.enfant.father.mother.lastName;
            this.scenario.enfant.father.mother.father.child = this.scenario.enfant.father.mother.firstName + " " + this.scenario.enfant.father.mother.lastName;
        }
        this.scenario.enfant.mother.mother.child = this.scenario.enfant.mother.firstName + " " + this.scenario.enfant.mother.lastName;
        this.scenario.enfant.mother.father.child = this.scenario.enfant.mother.firstName + " " + this.scenario.enfant.mother.lastName;
        this.scenario.enfant.father.mother.child = this.scenario.enfant.father.firstName + " " + this.scenario.enfant.father.lastName;
        this.scenario.enfant.father.father.child = this.scenario.enfant.father.firstName + " " + this.scenario.enfant.father.lastName;
        this.scenario.enfant.mother.child = this.scenario.enfant.firstName + " " + this.scenario.enfant.lastName;
        this.scenario.enfant.father.child = this.scenario.enfant.firstName + " " + this.scenario.enfant.lastName;
        this.scenario.enfant.child = undefined;
        //Setup Characters
        if (degree > 3) {
            characters.push(this.scenario.enfant.mother.mother.mother);
            characters.push(this.scenario.enfant.mother.mother.father);
            characters.push(this.scenario.enfant.mother.father.mother);
            characters.push(this.scenario.enfant.mother.father.father);
            characters.push(this.scenario.enfant.father.mother.mother);
            characters.push(this.scenario.enfant.father.mother.father);
            characters.push(this.scenario.enfant.father.father.mother);
            characters.push(this.scenario.enfant.father.father.father);
        }
        characters.push(this.scenario.enfant.mother.mother);
        characters.push(this.scenario.enfant.mother.father);
        characters.push(this.scenario.enfant.father.mother);
        characters.push(this.scenario.enfant.father.father);
        characters.push(this.scenario.enfant.mother);
        characters.push(this.scenario.enfant.father);
        characters.push(this.scenario.enfant);

        let correctOrder = characters.slice(0);

        //Add intruders
        if (this.options.numberOfIntruder)
            for (let i = 0; i < this.options.numberOfIntruder; i++)
                characters.push(intruders[i]);

        this.shuffleArray(characters);
        //First line
        if (degree > 3) {
            this.tree.addCells(50, 100, 50, 0, this.refGame.global.resources.getImage('cellw'));
            this.tree.addCells(105, 100, 50, 0, this.refGame.global.resources.getImage('cellm'));

            this.tree.addCells(185, 100, 50, 0, this.refGame.global.resources.getImage('cellw'));
            this.tree.addCells(240, 100, 50, 0, this.refGame.global.resources.getImage('cellm'));

            this.tree.addCells(360, 100, 50, 0, this.refGame.global.resources.getImage('cellw'));
            this.tree.addCells(415, 100, 50, 0, this.refGame.global.resources.getImage('cellm'));

            this.tree.addCells(495, 100, 50, 0, this.refGame.global.resources.getImage('cellw'));
            this.tree.addCells(550, 100, 50, 0, this.refGame.global.resources.getImage('cellm'));
        }

        //Second line
        this.tree.addCells(degree > 3 ? 118 : 113, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'));
        this.tree.addCells(degree > 3 ? 173 : 178, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'));

        this.tree.addCells(degree > 3 ? 427 : 422, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'));
        this.tree.addCells(degree > 3 ? 482 : 487, degree > 3 ? 200 : 100, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'));

        //Third line
        this.tree.addCells(degree > 3 ? 230 : 225, degree > 3 ? 300 : 250, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellw'));
        this.tree.addCells(degree > 3 ? 370 : 375, degree > 3 ? 300 : 250, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cellm'));

        //Fourth line
        this.tree.addCells(300, 400, degree > 3 ? 50 : 60, 0, this.refGame.global.resources.getImage('cell'));

        //Add characters
        let height = degree > 3 ? 55 : 65;
        let width = (height / 65) * 22;
        for (let i = 0; i < characters.length; i++) {
            let character = new Character(15 + i * 28, 520, width, height, this.refGame.global.resources.getImage(characters[i].image), characters[i], this.checkOnMove.bind(this), this.displayInfo.bind(this));
            this.tree.addCharacter(character, intruders.indexOf(characters[i]) > -1);
        }

        this.sortCharacters(correctOrder);
    }

    displayInfo(character) {
        let personData = character.info;
        let textToSpeech = '';
        if (this.options.showFirstName)
            textToSpeech += this.refGame.global.resources.getOtherText('firstName', {param: personData.firstName}) + ";";
        if (this.options.showLastName)
            textToSpeech += this.refGame.global.resources.getOtherText('lastName', {param: personData.lastName}) + " " + ";";
        if (this.options.showAge)
            textToSpeech += this.refGame.global.resources.getOtherText('age', {param: this.calculateAge(personData.birthdate, personData.deathdate)}) + " " + ";";
        if (this.options.showBirthdate)
            textToSpeech += this.refGame.global.resources.getOtherText('birthdate', {param: personData.birthdate}) + " " + ";";
        if (this.options.showDeathdate)
            textToSpeech += (personData.deathdate !== "" ? this.refGame.global.resources.getOtherText('deathdate', {param: personData.deathdate}) + " " : "") + ";";
        if (this.options.showAddress)
            textToSpeech += this.refGame.global.resources.getOtherText('address', {param: personData.address}) + " " + ";";
        if (this.options.showParents)
            textToSpeech += this.refGame.global.resources.getOtherText('parents', {param: personData.father.firstName + " " + personData.father.lastName + " & " + personData.mother.firstName + " " + personData.mother.lastName}) + " " + ";";
        if (this.options.showChild)
            textToSpeech += (personData.child && personData.child !== "" ? this.refGame.global.resources.getOtherText('child', {param: personData.child}) : "");

        let htmlAlert = "<img src='" + this.refGame.global.resources.getImage(personData.image).getImage().src + "' style='width:20%;'><br>";
        if (this.options.showFirstName)
            htmlAlert += this.refGame.global.resources.getOtherText('firstName', {param: personData.firstName}) + "<br>";
        if (this.options.showLastName)
            htmlAlert += this.refGame.global.resources.getOtherText('lastName', {param: personData.lastName}) + "<br>";
        if (this.options.showAge)
            htmlAlert += this.refGame.global.resources.getOtherText('age', {param: this.calculateAge(personData.birthdate, personData.deathdate)}) + "<br>";
        if (this.options.showBirthdate)
            htmlAlert += this.refGame.global.resources.getOtherText('birthdate', {param: personData.birthdate}) + "<br>";
        if (this.options.showDeathdate)
            htmlAlert += (personData.deathdate !== "" ? this.refGame.global.resources.getOtherText('deathdate', {param: personData.deathdate}) + "<br>" : "");
        if (this.options.showAddress)
            htmlAlert += this.refGame.global.resources.getOtherText('address', {param: personData.address}) + "<br>";
        if (this.options.showParents)
            htmlAlert += this.refGame.global.resources.getOtherText('parents', {param: personData.father.firstName + " " + personData.father.lastName + " & " + personData.mother.firstName + " " + personData.mother.lastName}) + "<br>";
        if (this.options.showChild)
            htmlAlert += (personData.child && personData.child !== "" ? this.refGame.global.resources.getOtherText('child', {param: personData.child}) + "<br>" : "");
        htmlAlert += "<i class=\"fas fa-volume-up fa-3x\" id=\"speaker\" onclick=\"audio.textToSpeechFromTxt('" + textToSpeech + "', '" + this.refGame.global.resources.getLanguage() + "');\"></i>";
        this.refGame.global.util.showAlertPersonnalized({
            title: personData.firstName + " " + personData.lastName,
            html: htmlAlert,
            text: ''
        });
    }

    calculateAge(birthDateString, deathDateString) {
        birthDateString = birthDateString.split('.').reverse().join('-');
        let birthdate = new Date(birthDateString);
        let age;
        let month;
        if (deathDateString !== "") {
            deathDateString = deathDateString.split('.').reverse().join('-');
            let deathdate = new Date(deathDateString);
            age = deathdate.getFullYear() - birthdate.getFullYear();
            month = deathdate.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && deathdate.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return age;
        } else {
            let today = new Date();
            age = today.getFullYear() - birthdate.getFullYear();
            month = today.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate())) {
                age = age - 1;
            }
            return age;
        }
    }

    checkOnMove(character) {
        if (this.canMove)
            if (this.tree.checkIsInCell(character)) {
                if (this.haveCellsSpriteInside()) {
                    this.canMove = false;
                    let count = this.checkWin();
                    if (this.evaluate) {
                        let mark = 0;
                        let barem = JSON.parse(this.refGame.global.resources.getEvaluation()).notes;
                        let perc = (count / this.tree.cells.length) * 100;
                        for (let b of barem) {
                            if (b.min <= perc && b.max >= perc) {
                                mark = b.note;
                            }
                        }
                        this.refGame.global.statistics.updateStats(mark);
                    }
                    let errorCount = this.tree.cells.length - count;
                    if (errorCount !== 0) {
                        this.refGame.global.util.showAlert(
                            'error',
                            this.refGame.global.resources.getOtherText('defeat', {
                                numberErrors: errorCount
                            }),
                            this.refGame.global.resources.getOtherText('error'),
                            undefined,
                            this.exercice ? this.displayInterfaceCallback.bind(this) : this.showTitle.bind(this));
                    } else {
                        this.refGame.global.util.showAlert(
                            'success',
                            this.refGame.global.resources.getOtherText('victory'),
                            this.refGame.global.resources.getOtherText('success'),
                            undefined,
                            this.exercice ? this.displayInterfaceCallback.bind(this) : this.showTitle.bind(this));
                    }
                }
            }

    }

    shuffleArray(array) {
        let currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    checkWin() {
        let charactersInfo = [];
        if (this.refGame.global.resources.getDegre() > 3) {
            charactersInfo.push(this.scenario.enfant.mother.mother.mother);
            charactersInfo.push(this.scenario.enfant.mother.mother.father);
            charactersInfo.push(this.scenario.enfant.mother.father.mother);
            charactersInfo.push(this.scenario.enfant.mother.father.father);
            charactersInfo.push(this.scenario.enfant.father.mother.mother);
            charactersInfo.push(this.scenario.enfant.father.mother.father);
            charactersInfo.push(this.scenario.enfant.father.father.mother);
            charactersInfo.push(this.scenario.enfant.father.father.father);
        }
        charactersInfo.push(this.scenario.enfant.mother.mother);
        charactersInfo.push(this.scenario.enfant.mother.father);
        charactersInfo.push(this.scenario.enfant.father.mother);
        charactersInfo.push(this.scenario.enfant.father.father);
        charactersInfo.push(this.scenario.enfant.mother);
        charactersInfo.push(this.scenario.enfant.father);
        charactersInfo.push(this.scenario.enfant);

        let correctCount = 0;
        for (let i = 0; i < this.tree.cells.length; i++) {
            if (this.tree.checkIsInSpecificCell(this.tree.characters[i], this.tree.cells[i])) {
                if (this.tree.characters[i].info === charactersInfo[i])
                    correctCount++;
            }
        }
        return correctCount;

    }

    sortCharacters(correctOrder) {
        let newOrder = [];
        for (let i = 0; i < correctOrder.length; i++) {
            let searchedCharacter = correctOrder[i];
            for (let character of this.tree.characters) {
                if (character.info === searchedCharacter) {
                    newOrder.push(character);
                    break;
                }
            }
        }
        this.tree.characters = newOrder;
    }

    haveCellsSpriteInside() {
        let count = 0;
        for (let cell of this.tree.cells) {
            for (let character of this.tree.characters) {
                if (this.tree.checkIsInSpecificCell(character, cell)) {
                    count++;
                    break;
                }
            }
            for (let character of this.tree.intruders) {
                if (this.tree.checkIsInSpecificCell(character, cell)) {
                    count++;
                    break;
                }
            }
        }
        return count === this.tree.cells.length;
    }

    displayInterfaceCallback() {
        this.refGame[this.interfaceCallback].show(true);
    }

    setupSignalement() {
        this.refGame.inputs.signal.onclick = function () {
            Swal.fire({
                title: this.refGame.global.resources.getOtherText('signalementTitle'),
                cancelButtonText: this.refGame.global.resources.getOtherText('signalementCancel'),
                showCancelButton: true,
                html:
                    '<label>' + this.refGame.global.resources.getOtherText('signalementReason') + '</label><textarea id="swal-reason" class="swal2-textarea"></textarea>' +
                    '<label>' + this.refGame.global.resources.getOtherText('signalementPassword') + '</label><input type="password" id="swal-password" class="swal2-input">',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-reason').val(),
                            $('#swal-password').val()
                        ])
                    })
                },
                onOpen: function () {
                    $('#swal-reason').focus()
                }
            }).then(function (result) {
                if (result && result.value) {
                    this.refGame.global.resources.signaler(result.value[0], result.value[1], this.currentExerciceId, function (res) {
                        Swal.fire(res.message, '', res.type);
                        if (res.type === 'success') {
                            this.init(this.evaluation);
                            this.show();
                        }
                    }.bind(this));
                }
            }.bind(this));
        }.bind(this);
    }
}
/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }

}
/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class CreateMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('create');
        this.refGame = refGame;
        this.setInterfaces({create: new Create(refGame),})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init() {
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show(isFromTrainMode = false) {
        this.interfaces.create.show(isFromTrainMode);
    }

}
/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class ExploreMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('explore');
        this.refGame = refGame;
        this.setInterfaces({explore: new Explore(refGame),})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init() {
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show() {
        this.interfaces.explore.show();
    }

}
/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class PlayMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('play');
        this.refGame = refGame;
        this.setInterfaces({play: new Play(refGame),})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init() {
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show() {
        this.interfaces.play.show();
    }

}
/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class TrainMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.interfaceCallback = undefined;
        this.setInterfaces({train: new Train(refGame),})
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init(evaluate, interfaceCallback=undefined) {
        this.evaluate = evaluate;
        this.interfaceCallback = interfaceCallback;
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show(exercice = undefined) {
        this.interfaces.train.show(this.evaluate,exercice,this.interfaceCallback);
    }

}
/**
 * @classdesc Classe principale du jeu
 * @author Vincent Audergon
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;
        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));

        this.scenes = {
            explore: new PIXI.Container(),
            train: new PIXI.Container(),
            create: new PIXI.Container(),
            play: new PIXI.Container()
        };

        this.inputs = {
            signal: document.getElementById('signal')
        };

        this.exploreMode = new ExploreMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreateMode(this);
        this.playMode = new PlayMode(this);
        this.oldGamemode = undefined;
    }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        new this.global.Log('success', `Chargement de la scène`).show();
        for (let input in this.inputs) {
            this.inputs[input].style.display = 'none';
        }
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {
            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }

    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.exploreMode.onLanguageChanged(lang);
        this.trainMode.onLanguageChanged(lang);
        this.createMode.onLanguageChanged(lang);
        this.playMode.onLanguageChanged(lang);
    }


    onFontChange(isOpendyslexic) {
    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        let gamemode = this.global.util.getGamemode();
        switch (gamemode) {
            case this.global.Gamemode.Evaluate:
                this.trainMode.init(true);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Train:
                this.trainMode.init(false);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Explore:
                this.exploreMode.init();
                this.exploreMode.show();
                break;
            case this.global.Gamemode.Create:
                this.createMode.init();
                this.createMode.show();
                break;
            case this.global.Gamemode.Play:
                this.playMode.init();
                this.playMode.show();
                break;
        }
        this.oldGamemode = gamemode;
    }
}
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.pixiApp = new PixiFramework().getApp();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
const game = new Game(global);
