define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Gamemode;
    (function (Gamemode) {
        Gamemode["Explore"] = "explore";
        Gamemode["Train"] = "train";
        Gamemode["Evaluate"] = "evaluate";
        Gamemode["Create"] = "create";		Gamemode["Play"] = "play";		
    })(Gamemode = exports.Gamemode || (exports.Gamemode = {}));
});
